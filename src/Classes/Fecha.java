package Classes;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.JOptionPane;

public class Fecha {

	private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	private String fetxa;
	private int dia;
	private int mes;
	private int a�o;

	public Fecha(String introduirfetxa){
			
		String[] SplitArray=null;
		String fechaforma=null;
		
		try{
			SplitArray=introduirfetxa.split("/");	
			this.dia = Integer.parseInt(SplitArray[0]);
			this.mes = Integer.parseInt(SplitArray[1]);
			this.a�o = Integer.parseInt(SplitArray[2]);
			fechaforma = dia +"/"+ mes +"/"+ a�o;//si llevem la fecha forma peta
			this.fetxa = fechaforma;
				
		}catch(Exception e){
			JOptionPane.showMessageDialog(null, "El valor introducido no es de tipo fecha","ERROR",JOptionPane.ERROR_MESSAGE);
				
		}
	}
	
	public SimpleDateFormat GetFormat() {
		return format;
	}

	// Getters
	
	public String GetFetxa() {
		return fetxa;
	}
	public int GetDia() {
		return dia;
	}
	public int GetMes() {
		return mes;
	}
	public int GetAny() {
		return a�o;
	}
	
	// Setters
	
	public void SetFormat(SimpleDateFormat format) {
		this.format = format;
	}

	public void SetFetxa(String fetxa) {
		
		String[] SplitArray=null;
		String fechaforma=null;

		try{
			SplitArray=fetxa.split("/");	
			this.dia = Integer.parseInt(SplitArray[0]);
			this.mes = Integer.parseInt(SplitArray[1]);
			this.a�o = Integer.parseInt(SplitArray[2]);
			fechaforma = dia +"/"+ mes +"/"+ a�o;//si llevem la fecha forma peta
			this.fetxa = fechaforma;//???segurament hi ha que llevaro perque no s'utilitza
				
		}catch(Exception e){
			JOptionPane.showMessageDialog(null, "El valor introducido no es de tipo fecha","ERROR",JOptionPane.ERROR_MESSAGE);
				
		}	
	}

	public void SetDia(int dia) {
		this.dia = dia;
	}

	public void SetMes(int mes) {
		this.mes = mes;
	}
	
	public void SetA�o(int a�o) {
		this.a�o = a�o;
	}
	
	//To String
	public String ToString (){	
		String cad = "";
		
		cad = this.GetDia() + "/" + this.GetMes() + "/" + this.GetAny();
			
		return cad;
	}
	
		
	public Calendar converStringToCalendar(){		
		Calendar date = Calendar.getInstance();

		try {
			date.set(this.GetAny(), this.GetMes(), this.GetDia());//obtinguem els ints per a passar a Calendar
			
		} catch (Exception e) {
			return date;
		}

		return date;
	}
		
	public int subtractDates(){//pillem el any passat ja a calendar que el usuari a introdu�t(string) i la fecha del sistema per a traure la diferencia
			
		int diff, any1, anysistema;
		Calendar cal1 = new GregorianCalendar();
		Calendar fecha = new GregorianCalendar();
	        
		cal1 = converStringToCalendar();
		any1 = cal1.get(Calendar.YEAR);
		anysistema = fecha.get(Calendar.YEAR);
			
		diff = anysistema - any1;
		System.out.println("diff: " + diff);
			
		return diff;
	}
		
	public int compareDate(Fecha fecha2){//comparem la fecha completa amb la fecha ultima introdu�da per l'usuari
			int resultado = 0;//son iguals
			
			Calendar cal1 = this.converStringToCalendar();
			Calendar cal2 = fecha2.converStringToCalendar();
			
			  if (cal1.before(cal2)) 
				  resultado = -1;
			  else if (cal1.after(cal2)) 
				  resultado = 1;
			  else if (cal1.compareTo(cal2)==0) 
				  resultado = 0;
			  
		  return resultado;
	}
		
	public boolean checkDates(){
		Calendar cal = this.converStringToCalendar();
		int mes = cal.get(Calendar.MONTH);
		int dia = cal.get(Calendar.DAY_OF_MONTH);
		int any = cal.get(Calendar.YEAR);
	 	  
		if ((any < 1930) || (any > 2017)){
			return false;         
        }
	    if ((mes < 0) || (mes > 11)){
	    	return false;
	    }
	    	int[] mesos = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

	         if (mes==1){
	        	 int febMax=28;
	             if (((any%4)==0) && ((any%100)!=0) || ((any % 400) == 0)){
	            	 febMax=29;
	             }
	             if ((dia<1)||(dia>febMax)){
	            	 return false;
	             }
	         }else if ((dia < 1)||(dia > mesos[mes])){
	        	 return false;
	         }
	         return true;
	  }    
}
