import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import Modules.SecondHandProducts.Model.Classes.secondHandProducts;
import Modules.SecondHandProducts.Utils.funciones_pideDatos;
import Utils.funciones;

public class Test {

	public static void main(String[] args) {
		
		JFrame ventana = new JFrame("www.ProductosSegundaMano.es BETA 1.0" );
	    ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//para terminar la app
	    ventana.setLocation(250, 200);
		FlowLayout layout = new FlowLayout(FlowLayout.CENTER, 15, 15);//coloca en fila
	    ventana.setLayout(layout);	     
	  	     
	    JPanel panel = new JPanel(layout);
	    panel.setSize(new Dimension(700, 50));
	    panel.setBackground(Color.ORANGE);
	    panel.setPreferredSize(new Dimension(700, 250));
	    ventana.add(panel);
	 
	     // Creamos el escuchador para los eventos de cada boton
	     ActionListener escuchador = new ActionListener() {
	        @SuppressWarnings("static-access")
			public void actionPerformed(ActionEvent ae) {
	             JButton boton = (JButton) ae.getSource();
	             String nombreBoton = boton.getText();
	 
	             switch (nombreBoton) {
	                 case "Instrumentos":
	                	panel.setBackground(Color.GREEN);
	                    JFrame vtnaDatosInstru  = new JFrame("Ingresar");
	                    vtnaDatosInstru.setLocation(1100, 200);
	                    vtnaDatosInstru.setLayout(new FlowLayout());

	                    //Creamos los componentes
	                    JButton botonEnviar = new JButton("Registrar");
	                    JTextField txtNom  = new JTextField(14);
	                    JTextField txtFcompra  = new JTextField(14);
	                    JTextField txtTipo  = new JTextField(14);
	                    JTextField txtMarca  = new JTextField(14);
	                    JTextField txtColor  = new JTextField(14);
	                    JLabel etiNom= new JLabel("Nombre: ");
	                    JLabel etiFcompra= new JLabel("Fecha compra inicial: ");
	                    JLabel etiTipo= new JLabel("Tipo: ");
	                    JLabel etiMarca= new JLabel("Marca: ");
	                    JLabel etiColor= new JLabel("Color: ");
	                    
	                    //A�adimos los componentes a la ventana
	                    vtnaDatosInstru.add(etiNom);
	                    vtnaDatosInstru.add(txtNom);
	                    vtnaDatosInstru.add(etiFcompra);
	                    vtnaDatosInstru.add(txtFcompra);
	                    vtnaDatosInstru.add(etiTipo);
	                    vtnaDatosInstru.add(txtTipo);
	                    vtnaDatosInstru.add(etiMarca);
	                    vtnaDatosInstru.add(txtMarca);
	                    vtnaDatosInstru.add(etiColor);
	                    vtnaDatosInstru.add(txtColor);
	                    vtnaDatosInstru.add(botonEnviar);
	                      	                    
	                    //Configuraciones de la ventana
	                    vtnaDatosInstru.setSize(200, 350);
	                    vtnaDatosInstru.setVisible(true);
	                    
	                    botonEnviar.addActionListener(new ActionListener(){
	                    	public void actionPerformed(ActionEvent e){
	                    	String nombre = txtNom.getText();
	                    	boolean rdo1 = funciones.ValidaStringFrame(nombre);	    
		                    String fcompra = txtFcompra.getText();
		                    boolean rdo2=funciones.ValidaStringFrame(fcompra);
		                    String tipo = txtTipo.getText();
		                    boolean rdo3=funciones.ValidaStringFrame(tipo);
		                    String marca = txtMarca.getText();
		                    boolean rdo4=funciones.ValidaStringFrame(marca);		                    
		                    String color = txtColor.getText();
		                    boolean rdo5 = funciones.ValidaStringFrame(color);
		                    
		                    if((rdo1)&&(rdo2)&&(rdo3)&&(rdo4)&&(rdo5)){
		                    	secondHandProducts p = new secondHandProducts (nombre,fcompra,tipo,marca,color);
			                    JOptionPane.showMessageDialog(null, p.toString());
		                    }		                    

	                    }});
	                    
	                    if(vtnaDatosInstru.EXIT_ON_CLOSE==-1)
	                    	vtnaDatosInstru.dispose();
	                    	            
	                    break;
	                 case "Electronica":
	                    panel.setBackground(Color.BLUE);
	                    break;
	                 case "Hogar":
	                    panel.setBackground(Color.BLACK);
	                    break;
	             }
	         }

	     };
	 
	     // BOTONES
	     String botones[] = {"Instrumentos", "Electronica", "Hogar"};
	 
	     for (String titlebutton : botones) {
	         JButton boton = new JButton(titlebutton);
	         boton.setSize(new Dimension(200, 150));
	         ventana.add(boton);
	         boton.addActionListener(escuchador);
	     }
	 
	   	     
	     // PANEL --> Area de Texto
	     String text = " Bienvenidos a nuestra web primera venta mundial de productos segunda mano"
	     + "Bienvenidos a nuestra web primera venta mundial de productos segunda mano"
	     + "Bienvenidos a nuestra web primera venta mundial de productos segunda mano"
	     + "Bienvenidos a nuestra web primera venta mundial de productos segunda mano\n"
	     + "Bienvenidos a nuestra web primera venta mundial de productos segunda mano "
	     + "Bienvenidos a nuestra web primera venta mundial de productos segunda mano"
	     + "Bienvenidos a nuestra web primera venta mundial de productos segunda mano.\n"
	     + "Bienvenidos a nuestra web primera venta mundial de productos segunda mano "
	     + "Bienvenidos a nuestra web primera venta mundial de productos segunda mano "
	     + "Bienvenidos a nuestra web primera venta mundial de productos segunda mano.\n";
	     JTextArea texto = new JTextArea(13, 60);
	     texto.setText(text);
	     texto.setLineWrap(true);//Hace que las l�neas corten en el l�mite del �rea texto.
	     texto.setWrapStyleWord(true);//deja espacio a los laterales
	     texto.setForeground(Color.WHITE);
	     texto.setBackground(Color.BLACK);
	 
	     // SCROLL
	     JScrollPane scroll = new JScrollPane(texto);//panel desplazable, las barran son solo visibles cuando se necesiten
	     panel.add(scroll);

	     // ETIQUETA
	     JLabel etiqueta = new JLabel("Hay " + botones.length +  " diferentes tipos de productos disponibles ");
	     etiqueta.setForeground(Color.RED);
	     etiqueta.setFont(new Font("Serif", Font.BOLD, 20));
	     //etiqueta.setLocation(10,25);
	     ventana.add(etiqueta);
	 
	     // END
	     ventana.setSize(730, 450);
	     ventana.setVisible(true);
	  }
		
	

}
