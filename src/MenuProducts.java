import javax.swing.JOptionPane;
import Modules.SecondHandProducts.Model.Dummies.Utils.funciones_dummies;
import Modules.SecondHandProducts.Model.Graphs.Statistic;
import Modules.SecondHandProducts.Utils.funciones_products;
import Utils.funciones_menu;

public class MenuProducts {

	public static void cargarCrud() {
		JOptionPane.showMessageDialog(null,"Bienvenido a productos de segunda mano","Version: 2.2 by Joan Montes Doria", JOptionPane.INFORMATION_MESSAGE);
		String[] tipo = { "Metal Wind Instrument","Smartphones","TV","Value Prices","Exit"};
		int op = 0;	
		
		funciones_dummies.cargarDummiesAdmin();
		
			do{
				op = funciones_menu.menu("Elije opcion","Menu Principal",tipo);	
				if (op==4)
					JOptionPane.showMessageDialog(null,"Bye!!!, deseamos que le haya gustado. Vuelva pronto", "EXIT",JOptionPane.INFORMATION_MESSAGE);
				
				else if (op==-1)
					JOptionPane.showMessageDialog(null,"Tiene que elejir una opcion", "Upss!",JOptionPane.ERROR_MESSAGE);
				
				else{					
					switch(op){
						case 0:
							funciones_products.CaseMetalWindInstrument();
						break;
							
						case 1:
							funciones_products.CaseSmartphones();
						break;
							
						case 2:
							funciones_products.CaseTvs();
						break;
						case 3:
							new Statistic();
						break;
					}
				}
				
			}while(op!=4);	
	}
}
