package Utils;

import javax.swing.JOptionPane;

public class funciones {

	public static int ValidaInt(String title, String message){
		int num = 0;
		boolean correct = true;
		String s;
		
		do{
			try{
				s=JOptionPane.showInputDialog(null,message,title,JOptionPane.QUESTION_MESSAGE);
				if(s==null){
					JOptionPane.showMessageDialog(null,"You haven't introduce a valid num","ERROR",JOptionPane.ERROR_MESSAGE);
					correct = false;
				}else {
					num = Integer.parseInt(s);
					correct = true;
				}
			}catch(Exception e){
				JOptionPane.showMessageDialog(null,"You haven't introduce a valid num", "ERROR",JOptionPane.ERROR_MESSAGE);
				correct = false;
			}
		}while(correct==false);
		
		return num;
	}
	
	public static int ValidaIntFrame(String title, String message){
		int num = 0;
		boolean correct = true;
		String s;
		
		do{
			try{
				s=JOptionPane.showInputDialog(null,message,title,JOptionPane.QUESTION_MESSAGE);
				if(s==null){
					JOptionPane.showMessageDialog(null,"You haven't introduce a valid num","ERROR",JOptionPane.ERROR_MESSAGE);
					correct = false;
				}else {
					num = Integer.parseInt(s);
					correct = true;
				}
			}catch(Exception e){
				JOptionPane.showMessageDialog(null,"You haven't introduce a valid num", "ERROR",JOptionPane.ERROR_MESSAGE);
				correct = false;
			}
		}while(correct==false);
		
		return num;
	}
	
	public static char ValidaChar (String title, String message){
		char lletra = 0;
		boolean correct = true;
		String a;
		int cont = 0;
		
		do{
			try{
				a = JOptionPane.showInputDialog(null, message,title,JOptionPane.QUESTION_MESSAGE);
				if(a==null){
					JOptionPane.showMessageDialog(null, "Empty","ERROR",JOptionPane.ERROR_MESSAGE);
					correct=false;
				}else if(a.isEmpty()){
					JOptionPane.showMessageDialog(null, "Empty","ERROR",JOptionPane.ERROR_MESSAGE);
					correct=false;
				}
				else{
					if(a.length()>1){
						JOptionPane.showMessageDialog(null, "You haven't introduce a valid letter(1 only character)","ERROR",JOptionPane.ERROR_MESSAGE);
						correct=false;
					}else{
						if(Character.isLetter(a.charAt(0))){
							lletra=a.charAt(0);
							correct=true; 
						}else{
							  JOptionPane.showMessageDialog(null, "You haven't introduce a valid letter","ERROR",JOptionPane.ERROR_MESSAGE);
							  correct=false;
						  }
					}
				}
			}catch(Exception e){
				JOptionPane.showMessageDialog(null, "You haven't introduce a valid letter","ERROR",JOptionPane.ERROR_MESSAGE);
				correct=false;
			}
		}while(correct==false);
			return lletra;
	}
	
	public static float ValidaFloat(String title, String message) {
		
		float num = 0.0f;
		String s;
		boolean correct = true;

		do{
			try{
				s=JOptionPane.showInputDialog(null, message,title,JOptionPane.QUESTION_MESSAGE);
				if(s==null){
					JOptionPane.showMessageDialog(null, "You haven't introduce a valid num","ERROR",JOptionPane.ERROR_MESSAGE);
					correct = false;
				}else {
					num = Float.parseFloat(s);
					correct = true;
				}
			}catch(Exception e){
				JOptionPane.showMessageDialog(null, "You haven't introduce a valid num","ERROR",JOptionPane.ERROR_MESSAGE);
				correct = false;
			}
		}while(correct == false);	
		
		return num;
	}
	
	public static String ValidaString(String title, String message){
		boolean correct = true;
		String s = "";
		
		do{
			try{
				s = JOptionPane.showInputDialog(null,message,title,JOptionPane.QUESTION_MESSAGE);
				correct = true;
				if (s==null){
					JOptionPane.showMessageDialog(null,"You haven't introduce a valid string","ERROR",JOptionPane.ERROR_MESSAGE);
					correct = false;
				}
				if(s.equals("")){
					JOptionPane.showMessageDialog(null,"The string is empty","ERROR",JOptionPane.ERROR_MESSAGE);
					correct = false;
				}
			}catch(Exception e){
				JOptionPane.showMessageDialog(null,"You haven't introduce a valid string","ERROR",JOptionPane.ERROR_MESSAGE);
				correct = false;
			}
		}while(correct==false);
		return s;
	}
	public static boolean ValidaStringFrameNom(String s){
		boolean correct = true;
		
			try{
				correct = true;
				if (s==null){
					JOptionPane.showMessageDialog(null,"You haven't introduce a valid string","ERROR",JOptionPane.ERROR_MESSAGE);
					correct = false;
				}
				if(s.equals("")){
					JOptionPane.showMessageDialog(null,"The string is empty","ERROR",JOptionPane.ERROR_MESSAGE);
					correct = false;
				}
				correct = validate_products.ValidaNomFrame(s);
				if(correct==false)
					JOptionPane.showMessageDialog(null, "Three characters minimum","Name",JOptionPane.ERROR_MESSAGE);
			}catch(Exception e){
				JOptionPane.showMessageDialog(null,"You haven't introduce a valid string","ERROR",JOptionPane.ERROR_MESSAGE);
				correct = false;
			}
		
		return correct;
	}
	
	public static boolean ValidaStringFramePass(String s){
		boolean correct = true;
		
			try{
				correct = true;
				if (s==null){
					JOptionPane.showMessageDialog(null,"You haven't introduce a valid string","ERROR",JOptionPane.ERROR_MESSAGE);
					correct = false;
				}
				if(s.equals("")){
					JOptionPane.showMessageDialog(null,"The string is empty","ERROR",JOptionPane.ERROR_MESSAGE);
					correct = false;
				}
				correct = validate_products.ValidaPasswd(s);
				if(correct==false){
					JOptionPane.showMessageDialog(null, "One uppercase letter and four digits","Password",JOptionPane.ERROR_MESSAGE);
				}
			}catch(Exception e){
				JOptionPane.showMessageDialog(null,"You haven't introduce a valid string","ERROR",JOptionPane.ERROR_MESSAGE);
				correct = false;
			}
		
		return correct;
	}
	
	public static void ordSelAsc(int[] arreglo) {  //ordena de manera ascendente (0,1,2,3)
		
        //iteramos sobre los elementos del arreglo
        for (int i = 0 ; i < arreglo.length - 1 ; i++) {
            int min = i;
 
            //buscamos el menor n�mero
            for (int j = i + 1 ; j < arreglo.length ; j++) {
                if (arreglo[j] < arreglo[min]) {
                    min = j;    //encontramos el menor n�mero
                }
            }
 
            if (i != min) {
                //permutamos los valores
                int aux = arreglo[i];
                arreglo[i] = arreglo[min];
                arreglo[min] = aux;
            }
        }
    }
	
	public static void ordSelDesc(int[] arreglo) { //ordena de manera descendente (9,8,7,6,5,4)
        //iteramos sobre los elementos del arreglo
        for (int i = 0 ; i < arreglo.length - 1 ; i++) {
            int max = i;
 
            //buscamos el mayor n�mero
            for (int j = i + 1 ; j < arreglo.length ; j++) {
                if (arreglo[j] > arreglo[max]) {
                    max = j;    //encontramos el mayor n�mero
                }
            }
 
            if (i != max) {
                //permutamos los valores
                int aux = arreglo[i];
                arreglo[i] = arreglo[max];
                arreglo[max] = aux;
            }
        }
	}
}
