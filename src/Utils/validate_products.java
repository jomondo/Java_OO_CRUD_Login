package Utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

public class validate_products {

	private static final String plantilla_cod ="([A-Z]{1}[0-9]{1})";
	private static final String plantilla_nom ="([a-zA-Z\\s]{3,25})";
	private static final String plantilla_adreca = "^*C/[a-zA-Z\\s]*N�([0-9]{1,2})*$";
	private static final String plantilla_tipo = "([a-zA-Z\\s]{3,25})";
	private static final String plantilla_email=("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
	private static final String plantilla_comment ="^[0-9]{1,5}$";
	private static final String plantilla_password="^([A-Z]{1})([0-9]{4})$";//1 letra en mayuscula y 4 digitos
	private static final String plantilla_puntos ="^[0-9]{1,5}$";
	private static final String plantilla_telefono ="^[0-9]{9}$";
	private static final String plantilla_precio ="^[0-9]{1,4}.[0-9]{1,4}$";
	private static final String plantilla_boquilla ="([A-Z]{1}[0-9]{1})";
	private static final String plantilla_campana ="^[0-9]{1,3}.[0-9]{1,2}$";
	private static final String plantilla_pulgadas ="([0-9]{1,2})";
	private static final String plantilla_bateria ="([0-9]{1,2})";
	private static final String plantilla_consumo ="([0-9]{1,3})";
	private static final String plantilla_capacidad ="([0-9]{1,4})";
	private static final String plantilla_fetxa1 = "(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/]((175[7-9])|(17[6-9][0-9])|(1[8-9][0-9][0-9])|([2-9][0-9][0-9][0-9]))";
	
	public static boolean ValidaNom(String nom){
		Pattern pattern=Pattern.compile(plantilla_nom);
		Matcher matcher = pattern.matcher(nom);
		if(matcher.matches()){
		return true;
		  } 
		JOptionPane.showMessageDialog(null,"Valor incorrecto. 3 caracteres minimo","ERROR",JOptionPane.ERROR_MESSAGE);
	    return false; 
	  }
	
	public static boolean ValidaNomFrame(String nom){
		Pattern pattern=Pattern.compile(plantilla_nom);
		Matcher matcher = pattern.matcher(nom);
		if(matcher.matches()){
		return true;
		  } 
		//JOptionPane.showMessageDialog(null,"Valor incorrecto. 3 caracteres minimo","ERROR",JOptionPane.ERROR_MESSAGE);
	    return false; 
	  }
	
	public static boolean ValidaCod(String cod){
		Pattern pattern=Pattern.compile(plantilla_cod);
		Matcher matcher = pattern.matcher(cod);
		if(matcher.matches()){
		return true;
		  } 
		JOptionPane.showMessageDialog(null,"ERROR","Valor incorrecto. One uppercase letter and one digit",JOptionPane.ERROR_MESSAGE);
	    return false; 
	  }
	
	public static boolean ValidaBoquilla(String boquilla){
		Pattern pattern=Pattern.compile(plantilla_boquilla);
		Matcher matcher = pattern.matcher(boquilla);
		if(matcher.matches()){
		return true;
		  } 
		JOptionPane.showMessageDialog(null,"ERROR","Valor incorrecto",JOptionPane.ERROR_MESSAGE);
	    return false; 
	  }
	
	public static boolean ValidaCapacidad(String capacidad){
		Pattern pattern=Pattern.compile(plantilla_capacidad);
		Matcher matcher = pattern.matcher(capacidad);
		if(matcher.matches()){
		return true;
		  } 
		JOptionPane.showMessageDialog(null,"ERROR","Valor incorrecto",JOptionPane.ERROR_MESSAGE);
	    return false; 
	  }
	
	public static boolean ValidaBateria(String bateria){
		Pattern pattern=Pattern.compile(plantilla_bateria);
		Matcher matcher = pattern.matcher(bateria);
		if(matcher.matches()){
		return true;
		  } 
		JOptionPane.showMessageDialog(null,"ERROR","Valor incorrecto",JOptionPane.ERROR_MESSAGE);
	    return false; 
	  }
	public static boolean ValidaConsumo(String consumo){
		Pattern pattern=Pattern.compile(plantilla_consumo);
		Matcher matcher = pattern.matcher(consumo);
		if(matcher.matches()){
		return true;
		  } 
		JOptionPane.showMessageDialog(null,"ERROR","Valor incorrecto",JOptionPane.ERROR_MESSAGE);
	    return false; 
	  }
	
	public static boolean ValidaPulgadas(String pulgadas){
		Pattern pattern=Pattern.compile(plantilla_pulgadas);
		Matcher matcher = pattern.matcher(pulgadas);
		if(matcher.matches()){
		return true;
		  } 
		JOptionPane.showMessageDialog(null,"ERROR","Valor incorrecto",JOptionPane.ERROR_MESSAGE);
	    return false; 
	  }
	
	public static boolean ValidaPrecio(String precio){
		Pattern pattern=Pattern.compile(plantilla_precio);
		Matcher matcher = pattern.matcher(precio);
		if(matcher.matches()){
		return true;
		  } 
		JOptionPane.showMessageDialog(null,"ERROR","Valor incorrecto",JOptionPane.ERROR_MESSAGE);
	    return false; 
	  }
	
	public static boolean ValidaCampana(String campana){
		Pattern pattern=Pattern.compile(plantilla_campana);
		Matcher matcher = pattern.matcher(campana);
		if(matcher.matches()){
		return true;
		  } 
		JOptionPane.showMessageDialog(null,"ERROR","Valor incorrecto",JOptionPane.ERROR_MESSAGE);
	    return false; 
	  }
	
	public static boolean ValidaDireccion(String adreca){
        Pattern pattern=Pattern.compile(plantilla_adreca);
        Matcher matcher = pattern.matcher(adreca);
		if(matcher.matches()){
		return true;
		  }
		JOptionPane.showMessageDialog(null,"ERROR","Valor incorrecto",JOptionPane.ERROR_MESSAGE);	    
		return false; 
	}//end_valida_adreca
	
	public static boolean ValidaComment(String comment){
        Pattern pattern=Pattern.compile(plantilla_comment);
        Matcher matcher = pattern.matcher(comment);
		if(matcher.matches()){
		return true;
		  }
		JOptionPane.showMessageDialog(null,"ERROR","Valor incorrecto",JOptionPane.ERROR_MESSAGE);    
		return false; 
	}//end_valida_adreca
	
	public static boolean validatipo(String tipo){
        Pattern pattern=Pattern.compile(plantilla_tipo);
        Matcher matcher = pattern.matcher(tipo);
		if(matcher.matches()){
		return true;
		  } 
	    return false; 
	}//end_valida_tipo
	
	public static boolean ValidaEmail(String email){
        Pattern pattern=Pattern.compile(plantilla_email);
        Matcher matcher = pattern.matcher(email);
		if(matcher.matches()){
		return true;
		  } 
		JOptionPane.showMessageDialog(null,"ERROR","Valor incorrecto",JOptionPane.ERROR_MESSAGE); 
		return false; 
	}//end_valida_email
	
	
	public static boolean ValidaPasswd(String password){
        Pattern pattern=Pattern.compile(plantilla_password);
        Matcher matcher = pattern.matcher(password);
		if(matcher.matches()){
		return true;
		  }
		return false; 
	}//end_valida_password
	
	public static boolean ValidaPuntos(String Puntos){
        Pattern pattern=Pattern.compile(plantilla_puntos);
        Matcher matcher = pattern.matcher(Puntos);
		if(matcher.matches()){
		return true;
		  } 
		JOptionPane.showMessageDialog(null,"ERROR","Valor incorrecto",JOptionPane.ERROR_MESSAGE);    
		return false; 
	  }//end_valida_puntos
	
	public static boolean ValidaTelf(String telefono){
        Pattern pattern=Pattern.compile(plantilla_telefono);
        Matcher matcher = pattern.matcher(telefono);
		if(matcher.matches()){
		return true;
		  } 
		JOptionPane.showMessageDialog(null,"ERROR","Valor incorrecto",JOptionPane.ERROR_MESSAGE);	    
		return false; 
	  }//end_valida_telef
	
	public static boolean ValidaFetxa(String fetxa){
        Pattern pattern=Pattern.compile(plantilla_fetxa1);
        Matcher matcher = pattern.matcher(fetxa);
		if(matcher.matches()){
		return true;
		  }
		JOptionPane.showMessageDialog(null,"ERROR","Valor incorrecto",JOptionPane.ERROR_MESSAGE);	    
		return false; 
	  }//end_valida_fetxa
	
	public static boolean ValidaDni(String DNI){ //Valida un DNI
		boolean val = false;
		String caracteres="TRWAGMYFPDXBNJZSQVHLCKET";
		int dni = 0;
		
		try{
			dni = Integer.parseInt(DNI.substring(0, DNI.length()-1));
			char letra = DNI.charAt(DNI.length()-1);	
			char let = caracteres.charAt(dni % 23);
			System.out.println("DNI: "+DNI+"\nN�mero: "+dni+"\nLetra: "+letra+"\nLet: "+let+"\nPosici�n: "+(dni%23));
	    	if(let == letra)
	    		val = true;
	    	else
	    		JOptionPane.showMessageDialog(null,"ERROR","Valor incorrecto(12345678A)",JOptionPane.ERROR_MESSAGE); 
		}catch(Exception e){
			JOptionPane.showMessageDialog(null,"ERROR","DNI incorrecto",JOptionPane.ERROR_MESSAGE);
		}
		
		
    	
    	return val; 
	}
}
