package Utils;

import javax.swing.JOptionPane;

public class funciones_menu {

	public static int menu(String title, String message, String tipo[]){
			
		int option = JOptionPane.showOptionDialog(null,
				title,message,0,
				JOptionPane.QUESTION_MESSAGE,null,tipo,tipo[0]);	
			
		
		return option;
	}
	

	public static String combobox(String title, String message, String tipo[]) {
		String op="";
		Object seleccion = JOptionPane.showInputDialog(null, title, message, JOptionPane.QUESTION_MESSAGE, null, tipo,tipo[0]);
		
		if(seleccion==null){
			op="";
		}else{
			op=(String) seleccion;
		}
		return op;
		
	}

}
