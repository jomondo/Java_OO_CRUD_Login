package Modules.SecondHandProducts.Model.Classes.Order;

import java.util.Comparator;
import Modules.SecondHandProducts.Model.Classes.secondHandProducts;


public class orderColor implements Comparator <secondHandProducts>{

	public int compare (secondHandProducts u1, secondHandProducts u2) {
		if((u1.getColor().compareTo(u2.getColor())>0))
			return 1;
		if((u1.getColor().compareTo(u2.getColor())<0))
			return -1;
		return 0;
	}
}
