package Modules.SecondHandProducts.Model.Classes.Order;

import java.util.Comparator;
import Modules.SecondHandProducts.Model.Classes.secondHandProducts;
import Modules.SecondHandProducts.Model.Classes.tvs;

public class orderConsumo implements Comparator <secondHandProducts>{

	public int compare (secondHandProducts u1, secondHandProducts u2) {
		if(((tvs)u1).getConsumo()>((tvs)u2).getConsumo())
			return 1;
		if(((tvs)u1).getConsumo()<((tvs)u2).getConsumo())
			return -1;
		return 0;
	}
}
