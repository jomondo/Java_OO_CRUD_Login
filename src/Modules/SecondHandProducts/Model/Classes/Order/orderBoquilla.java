package Modules.SecondHandProducts.Model.Classes.Order;

import java.util.Comparator;
import Modules.SecondHandProducts.Model.Classes.secondHandProducts;
import Modules.SecondHandProducts.Model.Classes.metalWindInstrument;

public class orderBoquilla implements Comparator <secondHandProducts>{
	public int compare (secondHandProducts u1, secondHandProducts u2) {
		if((((metalWindInstrument)u1).getBoquilla().compareTo(((metalWindInstrument)u2).getBoquilla())>0))
			return 1;
		if((((metalWindInstrument)u1).getBoquilla().compareTo(((metalWindInstrument)u2).getBoquilla())<0))
			return -1;
		return 0;
	}
}
