package Modules.SecondHandProducts.Model.Classes.Order;

import java.util.Comparator;
import Modules.SecondHandProducts.Model.Classes.secondHandProducts;
import Modules.SecondHandProducts.Model.Classes.tvs;

public class orderTipo implements Comparator <secondHandProducts>{

	public int compare (secondHandProducts u1, secondHandProducts u2) {
		if((((tvs)u1).getTipo().compareTo(((tvs)u2).getTipo())>0))
			return 1;
		if((((tvs)u1).getTipo().compareTo(((tvs)u2).getTipo())<0))
			return -1;
		return 0;
	}
}
