package Modules.SecondHandProducts.Model.Classes.Order;

import java.util.Comparator;
import Modules.SecondHandProducts.Model.Classes.secondHandProducts;
import Modules.SecondHandProducts.Model.Classes.metalWindInstrument;

public class orderMaterial implements Comparator <secondHandProducts> {

	public int compare (secondHandProducts u1, secondHandProducts u2) {
		if(((metalWindInstrument)u1).getMaterial().compareTo(((metalWindInstrument)u2).getMaterial())>0)
			return 1;//la cadena 1 es mayor que la cadena 2.
		if(((metalWindInstrument)u1).getMaterial().compareTo(((metalWindInstrument)u2).getMaterial())<0)
			return -1;//la cadena 1 es menor que la cadena 2.
		return 0;//son iguales
	}
	
}
