package Modules.SecondHandProducts.Model.Classes.Order;

import java.util.Comparator;
import Modules.SecondHandProducts.Model.Classes.secondHandProducts;
import Modules.SecondHandProducts.Model.Classes.smartphones;

public class orderPulgadas implements Comparator <secondHandProducts>{

	public int compare (secondHandProducts u1, secondHandProducts u2) {
		if(((smartphones)u1).getPulgadas()>((smartphones)u2).getPulgadas())
			return 1;
		if(((smartphones)u1).getPulgadas()<((smartphones)u2).getPulgadas())
			return -1;
		return 0;
	}
}
