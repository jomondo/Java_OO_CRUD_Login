package Modules.SecondHandProducts.Model.Classes;

import Classes.Fecha;
import Modules.SecondHandProducts.Utils.funciones_private;

public class smartphones extends secondHandProducts{

	private int pulgadas;
	private int duracionBat;
	private int pixelesCamara;
	private String compa�ia;
	private String cpu;
	private int ram;
	private String sistema;
	private int capacidad;
	private Fecha fUpdate;
	
	public smartphones(String cod, String nombre, Fecha fcompra,Fecha fUpdate, String marca, String color, String DNI, float precio, int pulgadas, int duracionBat,int pixelesCamara,String compa�ia,String cpu,
			int ram,String sistema,int capacidad) {
		super(cod, nombre, fcompra, marca, color, DNI, precio);
		
		this.pulgadas = pulgadas;
		this.duracionBat = duracionBat;
		this.pixelesCamara = pixelesCamara;
		this.compa�ia = compa�ia;
		this.cpu = cpu;
		this.ram = ram;
		this.sistema = sistema;
		this.capacidad = capacidad;
		this.fUpdate = fUpdate;
		
	}
	
	public smartphones() {
		super();
	}
	
	/*
	 * primary key building
	 * 
	 */
	public smartphones(String cod) {
		super(cod);
	}
	
	public String CalculaPromocion (String marca){	
		return funciones_private.promocionSmartphone(marca);		
	}
//depen dels anys valdra un % mes o menos del preu
	public int CalculaUpdate (Fecha fUpdate){	
		int edad = fUpdate.subtractDates();
		return edad;		
	}
	
	public float CalculaPrecio (int edadSoftw, float precio){	
		return funciones_private.incrementoPrecio(edadSoftw,precio);		
	}

	public Fecha getfUpdate() {
		return fUpdate;
	}

	public int getPulgadas() {
		return pulgadas;
	}

	public int getDuracionBat() {
		return duracionBat;
	}

	public int getPixelesCamara() {
		return pixelesCamara;
	}

	public String getCompa�ia() {
		return compa�ia;
	}

	public String getCpu() {
		return cpu;
	}

	public int getRam() {
		return ram;
	}

	public String getSistema() {
		return sistema;
	}

	public int getCapacidad() {
		return capacidad;
	}




	public void setPulgadas(int pulgadas) {
		this.pulgadas = pulgadas;
	}

	public void setDuracionBat(int duracionBat) {
		this.duracionBat = duracionBat;
	}

	public void setPixelesCamara(int pixelesCamara) {
		this.pixelesCamara = pixelesCamara;
	}

	public void setCompa�ia(String compa�ia) {
		this.compa�ia = compa�ia;
	}

	public void setCpu(String cpu) {
		this.cpu = cpu;
	}

	public void setRam(int ram) {
		this.ram = ram;
	}

	public void setSistema(String sistema) {
		this.sistema = sistema;
	}

	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}

	public void setfUpdate(Fecha fUpdate) {
		this.fUpdate = fUpdate;
		CalculaPrecio(CalculaUpdate(this.fUpdate),this.getPrecio());
	}

	
	public String toString() {
		String cad = "";
		
		cad = cad + ("Codigo: " + this.getCod() + "\n");
		cad = cad + ("Producto: " + this.getNombre() + "\n");
		cad = cad + ("DNI antiguo propietario: " + this.getDNI() + "\n");
		cad = cad + ("Fecha compra: " + this.getFfabricacion().ToString() + "\n");
		cad = cad + ("Antiguedad: " + this.CalculaAnt(this.getFfabricacion()) + "\n");
		cad = cad + ("Fecha ultima actualizacion: " + this.getfUpdate().ToString() + "\n");
		cad = cad + ("Ultima actualizacion: " + "Hace "+this.CalculaUpdate(this.getfUpdate()) + " a�os" + "\n");
		cad = cad + ("Marca: " + this.getMarca() + "\n");
		cad = cad + ("Color: " + this.getColor() + "\n");
		cad = cad + ("Capacidad: " + this.getCapacidad() + "\n");
		cad = cad + ("Compa�ia: " + this.getCompa�ia() + "\n");
		cad = cad + ("CPU: " + this.getCpu() + "\n");
		cad = cad + ("Duracion bateria: " + this.getDuracionBat() + "\n");
		cad = cad + ("Mpx Camara: " + this.getPixelesCamara() + "\n");
		cad = cad + ("Pulgadas: " + this.getPulgadas() + "\n");
		cad = cad + ("Ram: " + this.getRam() + "\n");
		cad = cad + ("Sistema: " + this.getSistema() + "\n");
		cad = cad + ("Regalo: " + this.CalculaPromocion(this.getMarca()) + "\n");
		cad = cad + ("Precio: " + CalculaPrecio(CalculaUpdate(this.fUpdate),this.getPrecio()) + "\n");
		
		return cad;
	}

	
}
