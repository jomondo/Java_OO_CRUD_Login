package Modules.SecondHandProducts.Model.Classes;

import Classes.Fecha;

public class metalWindInstrument extends secondHandProducts{

	private String funda;
	private String material;
	private String boquilla;
	private String afinacion;
	private float campana;
	private String calibre;
	private Fecha FMante;
	private int mantenimiento;
	
	//si canviem la fecha de mantenimento no trau l'antiguetat be
	
	public metalWindInstrument(String cod, String nombre, Fecha fcompra,Fecha FMante, String marca, String color, String DNI, float precio, String material, String afinacion, String boquilla, float campana, 
			String calibre, String funda) {
	super(cod, nombre, fcompra, marca, color, DNI, precio);
		
		this.material = material;
		this.afinacion = afinacion;
		this.boquilla = boquilla;
		this.campana = campana;
		this.calibre = calibre;
		this.funda = funda;
		this.FMante = FMante;
		this.mantenimiento = CalculaMante();
		
	}


	public metalWindInstrument() {
		super();
	}
	
	public metalWindInstrument(String cod) {
		super(cod);//Clave pimaria
	}
	
	public int CalculaMante (){	
		int edad = FMante.subtractDates();
		
		return edad;		
	}
	
	
	public Fecha getFMante() {
		return FMante;
	}

	public String getFunda() {
		return funda;
	}


	public String getMaterial() {
		return material;
	}


	public String getBoquilla() {
		return boquilla;
	}


	public String getAfinacion() {
		return afinacion;
	}


	public float getCampana() {
		return campana;
	}


	public String getCalibre() {
		return calibre;
	}


	
	public void setFunda(String funda) {
		this.funda = funda;
	}


	public void setMaterial(String material) {
		this.material = material;
	}


	public void setBoquilla(String boquilla) {
		this.boquilla = boquilla;
	}


	public void setAfinacion(String afinacion) {
		this.afinacion = afinacion;
	}


	public void setCampana(float campana) {
		this.campana = campana;
	}


	public void setCalibre(String calibre) {
		this.calibre = calibre;
	}

	public void setFMante(Fecha fMante) {
		this.FMante = fMante;
	}

	public String toString() {
		String cad = "";
		
		cad = cad + ("Codigo: " + this.getCod() + "\n");
		cad = cad + ("Producto: " + this.getNombre() + "\n");
		cad = cad + ("DNI antiguo propietario: " + this.getDNI() + "\n");
		cad = cad + ("Fecha compra: " + this.getFfabricacion().ToString() + "\n");
		cad = cad + ("Antiguedad: " + this.CalculaAnt(this.getFfabricacion()) + "\n");
		cad = cad + ("Ultima fecha mantenimiento: " + this.getFMante().ToString() + "\n");
		cad = cad + ("Revision: " + this.CalculaMante() + " a�o/s sin revisar"+"\n");
		cad = cad + ("Marca: " + this.getMarca() + "\n");
		cad = cad + ("Color: " + this.getColor() + "\n");
		cad = cad + ("Afinacion: " + this.getAfinacion() + "\n");
		cad = cad + ("N� boquilla: " + this.getBoquilla() + "\n");
		cad = cad + ("Calibre: " + this.getCalibre() + "\n");
		cad = cad + ("Campana: " + this.getCampana() + "\n");
		cad = cad + ("Material: " + this.getMaterial() + "\n");
		cad = cad + ("Funda: " + this.getFunda() + "\n");
		cad = cad + ("Precio: " + this.getPrecio() + "�\n");
		
		return cad;
	}

}
