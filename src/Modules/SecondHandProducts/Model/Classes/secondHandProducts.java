package Modules.SecondHandProducts.Model.Classes;

import Classes.Fecha;

public abstract class secondHandProducts implements Comparable <secondHandProducts>{

	private String cod;
	private String nombre;
	private Fecha Ffabricacion;
	private String marca;
	private String color;
	private float precio;
	private String DNI;//el dni del antic propietari
	
	public secondHandProducts ( String cod,String nombre, Fecha Ffabricacion, String marca, String color,String DNI, float precio ){
		
		this.cod = cod;
		this.nombre = nombre;
		this.Ffabricacion = Ffabricacion;
		this.marca = marca;
		this.color = color;
		this.precio = precio;
		this.DNI = DNI;
	}
	
	public int CalculaAnt (Fecha fecha2){	
		int edad = fecha2.subtractDates();
		
		return edad;		
	}
	
	public secondHandProducts() {
	}
	
	public secondHandProducts(String cod) {
		this.cod = cod;
	}
	
	public String getCod() {
		return cod;
	}
	public String getNombre() {
		return nombre;
	}

	public Fecha getFfabricacion() {
		return Ffabricacion;
	}

	public String getMarca() {
		return marca;
	}
	public String getColor() {
		return color;
	}
	public float getPrecio() {
		return precio;
	}
	public String getDNI() {
		return DNI;
	}


	public void setCod(String cod) {
		this.cod = cod;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setFfabricacion(Fecha Ffabricacion) {
		this.Ffabricacion = Ffabricacion;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public void setPrecio(float precio) {
		this.precio = precio;
	}
	public void setDNI(String DNI) {
		this.DNI = DNI;
	}
	
	
	public int compareTo(secondHandProducts param) {//para ordenar los empleados por nombre
		if(this.getCod().compareTo(param.getCod())>0)
			return 1;
		if(this.getCod().compareTo(param.getCod())<0)
			return -1;
		return 0;
	  }
	
	public boolean equals(Object param){
		return getCod().equals(((secondHandProducts)param).getCod());
	}	
	
	public abstract String toString();
}
