package Modules.SecondHandProducts.Model.Classes;

import Classes.Fecha;
import Modules.SecondHandProducts.Utils.funciones_private;

public class tvs extends secondHandProducts{

	private int pulgadas;
	private String hd;
	private String led;
	private String ahorroInteligente;
	private String tipo;
	private int consumo;
	private String conexion;
	
	public tvs(String cod,String nombre, Fecha fcompra, String marca, String color, String DNI, float precio, int pulgadas, String hd, String led, String ahorroInteligente, String tipo
			, int consumo, String conexion) {
		super(cod, nombre, fcompra, marca, color, DNI, precio);
		
		this.tipo = tipo;
		this.ahorroInteligente = ahorroInteligente;
		this.conexion = conexion;
		this.consumo = consumo;
		this.hd = hd;
		this.led = led;
		this.pulgadas = pulgadas;

	}
	
	public tvs() {
		super();
	}
	
	public int calculaDescuento (){	
		int p = 0;
		int m = funciones_private.descuentoTvs(this.getMarca());
		
		if(m==0)
			p = 0;	
		else
			p = ((int) this.getPrecio()*m/100);
		
		return p;
	}
	public String mensajeDescuento (int p){	
		String mensaje = "";
		if(p==0){
			mensaje = "No se aplica a este precio -> ";
		}else
			mensaje = "";
		
		return mensaje;
	}
	
	public tvs(String cod) {
		super(cod);
	}

	public int getPulgadas() {
		return pulgadas;
	}


	public String getHd() {
		return hd;
	}

	public String getLed() {
		return led;
	}

	public String getAhorroInteligente() {
		return ahorroInteligente;
	}

	public String getTipo() {
		return tipo;
	}

	public int getConsumo() {
		return consumo;
	}

	public String getConexion() {
		return conexion;
	}

	
	
	public void setPulgadas(int pulgadas) {
		this.pulgadas = pulgadas;
	}


	public void setHd(String hd) {
		this.hd = hd;
	}

	public void setLed(String led) {
		this.led = led;
	}

	public void setAhorroInteligente(String ahorroInteligente) {
		this.ahorroInteligente = ahorroInteligente;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public void setConsumo(int consumo) {
		this.consumo = consumo;
	}

	public void setConexion(String conexion) {
		this.conexion = conexion;
	}

	
	public String toString() {
		String cad = "";
		
		cad = cad + ("Codigo: " + this.getCod() + "\n");
		cad = cad + ("Producto: " + this.getNombre() + "\n");
		cad = cad + ("DNI antiguo propietario: " + this.getDNI() + "\n");
		cad = cad + ("Fecha compra: " + this.getFfabricacion().ToString() + "\n");
		cad = cad + ("Antiguedad: " + this.CalculaAnt(this.getFfabricacion()) + "\n");
		cad = cad + ("Marca: " + this.getMarca() + "\n");
		cad = cad + ("Color: " + this.getColor() + "\n");
		cad = cad + ("Ahorro Inteligente: " + this.getAhorroInteligente() + "\n");
		cad = cad + ("Conexion: " + this.getConexion() + "\n");
		cad = cad + ("Consumo: " + this.getConsumo() + "W\n");
		cad = cad + ("HD: " + this.getHd() + "\n");
		cad = cad + ("Led: " + this.getLed() + "\n");
		cad = cad + ("Pulgadas: " + this.getPulgadas() + "''\n");
		cad = cad + ("Tipo TV: " + this.getTipo() + "\n");
		cad = cad + ("Precio Normal: " + this.getPrecio() + "�\n");
		cad = cad + ("Descuento: " + funciones_private.descuentoTvs(this.getMarca()) + "%\n");
		cad = cad + ("Precio con Descuento: " + mensajeDescuento(calculaDescuento()) + (this.getPrecio()-calculaDescuento()) + "�\n");
		
		return cad;
	}

}
