package Modules.SecondHandProducts.Model.Dummies;

import java.util.Random;

import Classes.Fecha;
import Classes.Singleton;
import Modules.SecondHandProducts.Model.Classes.metalWindInstrument;
import Modules.SecondHandProducts.Model.Classes.smartphones;
import Modules.SecondHandProducts.Model.Classes.tvs;

public class Dummies {

	public static String dni(int dni) {
		String NIF_STRING_ASOCIATION = "TRWAGMYFPDXBNJZSQVHLCKE";
        return String.valueOf(dni) + NIF_STRING_ASOCIATION.charAt(dni % 23);
    }
	
	public static String DNI (){
		String cad = "";
        long time = new java.util.GregorianCalendar().getTimeInMillis();
        Random random = new Random(time);
        while (cad.length() < 8) {
            char c = (char) random.nextInt(255);
            if ((c >= '0' && c <= '9')) {
                cad += c;
            }
        }
        int numberdni = Integer.parseInt(cad);
        return dni(numberdni);
	}
	
	public static String marcaTV (){
		String marcatv = "";
		String[] marcas = {"LG", "Samsung", "Sony", "JVC", "Sanyo", "Philips", "Sharp", "Technics", "Benq",
	            "Panasonic", "Airis", "Toshiba", "OKI", "Thomson"};
		
		int position = (int) (Math.random() * 222) % 13;
		return marcatv = marcas[position];
	}
	public static String nombreTV (){
		String nombretv = "";
		String[] nombres = {"TV U550C","TV A560C","TV J560","TV F560","TV WW4","TV K90","TV LO9","TV CI6","TV H89"};
		
		int position = (int) (Math.random() * 222) % 8;
		return nombretv = nombres[position];
	}
	public static String marcaPhone (){
		String marcaphone = "";
		String[] marcas = {"LG", "Samsung", "Asus", "Xiaomi", "BQ", "Dogee", "Motorola", "Huawei", "Sony Ericsson",
	            "Nokia", "BlackBerry", "Alcatel", "HTC", "iphone"};
		
		int position = (int) (Math.random() * 222) % 13;
		return marcaphone = marcas[position];
	}
	public static String siOno (){
		String siOno = "";
		String[] siono = {"Si", "No"};
		
		int position = (int) (Math.random() * 222) % 2;
		return siOno = siono[position];
	}
	public static String material (){
		String funda = "";
		String[] siono = {"Laton", "Laton Dorado", "Plata", "Ba�o de plata","Oro de 9k", "Oro de 24k","Platino"};
		
		int position = (int) (Math.random() * 222) % 6;
		return funda = siono[position];
	}
	public static String boquilla (){
		String boquilla = "";
		String[] tipos = {"7C", "6L", "5A", "7U","9K", "L0","A9"};
		
		int position = (int) (Math.random() * 222) % 6;
		return boquilla = tipos[position];
	}
	public static String cod (){
		String cod = "";
		String[] let = {"A1", "B2", "C3", "D4","E5", "F6","G7","H8","I9","J2","K5","L6","M7","N9","O1","P3","Q4","R2",
				"S3","T4","W5","X6","Y7","Z8","A2","B3","C4","D5","E6","F7","G8","H9","I1","J2","K3","L4","M5","N6","O7","P8","Q9","R1","S2","T3","W4"};
		
		int position1 = (int) (Math.random() * 44);
		//hi ha que canviaro??
		return cod = let[position1];
	}
	public static String afinacion (){
		String afinacion = "";
		String[] afi = {"Sib", "Do", "Sol", "Fa#","Mi", "Mib","La"};
		
		int position = (int) (Math.random() * 222) % 6;
		return afinacion = afi[position];
	}
	public static String tipos (){
		String tipostv = "";
		String[] tipos = {"Smart TV","4K/UHD","Curvos","QLED"};
		
		int position = (int) (Math.random() * 222) % 3;
		return tipostv = tipos[position];
	}
	
	public static String NombrePhone (){
		String marcaphone = "";
		String[] nombres = {"Smartphone 5X","Smartphone 4X","Smartphone Plus","Smartphone 8F","Smartphone A8","Smartphone Mega","Smartphone O8"};
		
		int position = (int) (Math.random() * 222) % 6;
		return marcaphone = nombres[position];
	}
	
	public static String marcaInstruments (){
		String marcainstruments = "";
		String[] marcas = {"Bach", "Yamaha", "B&S", "Schilke", "Thomann", "Adams", "CarolBrass", "Gerd Dowids", "Johannes Scherzer"};
		
		int position = (int) (Math.random() * 222) % 8;
		return marcainstruments = marcas[position];
	}
	public static String calibre (){
		String calibre = "";
		String[] cal = {"Bajo", "Medio-bajo", "Medio", "Medio-alto", "Alto"};
		
		int position = (int) (Math.random() * 222) % 4;
		return calibre = cal[position];
	}
	
	public static String nombreInstruments (){
		String nombreinstruments = "";
		String[] nombres = {"Trompeta", "Tuba", "Trombon", "Bombardino", "Trompa", "Fliscorno", "Trompas Tenor", "Baritono", "Trompa Alto"};
		
		int position = (int) (Math.random() * 222) % 8;
		return nombreinstruments = nombres[position];
	}
	public static String compa�ia (){
		String compa�ias = "";
		String[] nombres = {"Movistar", "Vodafone", "Symio", "Amena", "Yoigo", "Orange", "Lowis", "Tuenti", "Jazztel"};
		
		int position = (int) (Math.random() * 222) % 8;
		return compa�ias = nombres[position];
	}
	public static String sistema (){
		String sistemas = "";
		String[] nombres = {"Android","Windows Phone","iOS", "BlackBerry OS", "Otro"};
		
		int position = (int) (Math.random() * 222) % 4;
		return sistemas = nombres[position];
	}
	public static String conexion (){
		String conexion = "";
		String[] nombres = {"Wifi","Ethernet","Wifi y Ethernet","No tiene"};
		
		int position = (int) (Math.random() * 222) % 4;
		return conexion = nombres[position];
	}
	public static String cpu (){
		String cpus = "";
		String[] nombres = {"Qualcomm Snapdragon 835","Snapdragon 820","Leadcore", "Apple A10", "Apple A9","Kirin 955", "MTK Helio X20", "Samsung Exynos 7420","Otro"};
		
		int position = (int) (Math.random() * 222) % 8;
		return cpus = nombres[position];
	}
	
	
	public static String colores (){
		String color = "";
		String[] colores = {"Negro", "Blanco", "Rojo", "Amarillo", "Azul", "Verde", "Marron", "Violeta", "Gris",
	            "Cian", "Magenta", "Escarlata", "Carmesi", "Bermellon", "Granate", "Carmin", "Amaranto", "Pistacho", "Verde Oliva",
	            "Xanadu", "Plata", "Plata metalizado", "Zafiro"};
		
		int position = (int) (Math.random() * 222) % 22;
		return color = colores[position];
	}
	
	public static boolean state (){
		boolean state;
		boolean [] states = {true, false};
		
		int position = (int) (Math.random() * 222) % 2;
		return state = states[position];
	}
	
	public static int consumo (){
		int numAleatorio = 0,num1 = 1,num2 = 999;
		
			numAleatorio=(int)Math.floor(Math.random()*(num1-num2)+num2);
     
        return numAleatorio;
	}
	
	public static int precio (){
		int numAleatorio = 0,num1 = 1,num2 = 99999;
		for (int i=0;i<1;i++){
			numAleatorio=(int)Math.floor(Math.random()*(num1-num2)+num2);
            
        }
        
        return numAleatorio;
	}
	public static int capacidad (){
		int numAleatorio = 0,num1 = 1,num2 = 9999;
		for (int i=0;i<1;i++){
			numAleatorio=(int)Math.floor(Math.random()*(num1-num2)+num2);
            
        }
        
        return numAleatorio;
	}
	
	
	public static int ram (){
		int numAleatorio = 0,num1 = 1,num2 = 30;
		for (int i=0;i<1;i++){
			numAleatorio=(int)Math.floor(Math.random()*(num1-num2)+num2);
            
        }
        
        return numAleatorio;
	}
	
	public static int Bateria (){
		int numAleatorio = 0,num1 = 1,num2 = 80;
		for (int i=0;i<1;i++){
			numAleatorio=(int)Math.floor(Math.random()*(num1-num2)+num2);
            
        }
        
        return numAleatorio;
	}
	public static int pixeles (){
		int numAleatorio = 0,num1 = 1,num2 = 20;
		for (int i=0;i<1;i++){
			numAleatorio=(int)Math.floor(Math.random()*(num1-num2)+num2);
            
        }
        
        return numAleatorio;
	}
	
	public static int pulgadas (){
		int numAleatorio = 0,num1 = 5,num2 = 50;
		for (int i=0;i<1;i++){
			numAleatorio=(int)Math.floor(Math.random()*(num1-num2)+num2);
            
        }
        
        return numAleatorio;
	}
	
	public static char Sexo (){
		char email = 0;
		char[] emails = {'D','H'};
		
		int position = (int) (Math.random() * 222) % 2;
		return email = emails[position];
	}
	
	public static Fecha DateContract (){
		int day = (int) (Math.random() * (1 - 31) + 31);
		int month = (int) (Math.random() * (1 - 12) + 12);
		int year = (int) (Math.random() * (1930 - 2017) + 2017);
		String date = "";
		
		if ((day<10)||(month<10)){
			if(day<10)
				 date = "0"+day+"/"+month +"/"+year;
			if(month<10)
				 date = day+"/"+"0"+month +"/"+year;
			if ((month<10)&&(day<10))
				 date = "0"+day+"/"+"0"+month +"/"+year;
			
		}else
			date = day+"/"+month +"/"+year;
		
		return  new Fecha (date);
	}
	public static Fecha DateUp (){
		int day = (int) (Math.random() * (1 - 31) + 31);
		int month = (int) (Math.random() * (1 - 12) + 12);
		int year = (int) (Math.random() * (1930 - 2017) + 2017);
		String date = "";
		
		if ((day<10)||(month<10)){
			if(day<10)
				 date = "0"+day+"/"+month +"/"+year;
			if(month<10)
				 date = day+"/"+"0"+month +"/"+year;
			if ((month<10)&&(day<10))
				 date = "0"+day+"/"+"0"+month +"/"+year;
			
		}else
			date = day+"/"+month +"/"+year;
		
		return  new Fecha (date);
	}
	public static Fecha Date (){
		int day = (int) (Math.random() * (1 - 31) + 31);
		int month = (int) (Math.random() * (1 - 12) + 12);
		int year = (int) (Math.random() * (1962 - 2017) + 2017);
		String date = "";
		Fecha Date = null;
		
		if ((day<10)||(month<10)){
			if(day<10)
				 date = "0"+day+"/"+month +"/"+year;
			if(month<10)
				 date = day+"/"+"0"+month +"/"+year;
			if ((month<10)&&(day<10))
				 date = "0"+day+"/"+"0"+month +"/"+year;
			
		}else
			date = day+"/"+month +"/"+year;
			Date =  new Fecha (date);
			
		return  Date;
	}
	
	public static float campana (){
		float salary = (float) (Math.random() * (60.9 - 200.89) + 2000);
		return (float) Math.rint(salary * 100) / 100;
	}
	
	public static float incentive (){
		float incentive = (float) (Math.random() * (200 - 500) + 500);
		return (float) Math.rint(incentive * 100) / 100;
	}
	
	public static int activity (){
		int activity = (int) (Math.random() * (10 - 100) + 100);
		return (int) Math.rint(activity * 100) / 100;
	}
	
	public static boolean premium (){
		boolean premium;
		boolean [] premiums = {true, false};
		
		int position = (int) (Math.random() * 222) % 2;
		return premium = premiums[position];
	}
	
	
	public static void DummiesInstruments () {
		for (int i=0; i<5; i++) {
			metalWindInstrument I = new metalWindInstrument (cod (),nombreInstruments(), Date(),Date(), marcaInstruments (), colores (), DNI (),precio (),material(), afinacion(), boquilla(),campana(),calibre(),siOno());
			Singleton.metalWindInstrument.add(I);
		}
	}
	
	public static void DummiesPhones () {
		for (int i=0; i<5; i++) {
			smartphones S = new smartphones(cod (),NombrePhone(), Date (),Date(), marcaPhone (), colores (),DNI(), precio(), pulgadas(), Bateria(), pixeles (), compa�ia(),cpu(),ram(),sistema(),capacidad());
			Singleton.smartphones.add(S);
		}

	}
	
	public static void DummiesTvs () {
		for (int i=0; i<5; i++) {
			tvs T = new tvs(cod (),nombreTV(), Date (), marcaTV (), colores (), DNI (),precio (),pulgadas(), siOno(), siOno (),siOno(),tipos(),consumo(),conexion());
			Singleton.tvs.add(T);
		}
	}
}
