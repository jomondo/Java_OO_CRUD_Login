package Modules.SecondHandProducts.Model.Dummies.Utils;

import java.util.ArrayList;
import javax.swing.JOptionPane;
import Classes.Singleton;
import Modules.SecondHandProducts.Model.Classes.metalWindInstrument;
import Modules.SecondHandProducts.Model.Classes.smartphones;
import Modules.SecondHandProducts.Model.Classes.tvs;
import Modules.SecondHandProducts.Model.Dummies.Dummies;
import Utils.funciones;
import Utils.funciones_menu;

public class funciones_dummies {

	public static void cargarDummiesAdmin(){
		//declarades public static en singleton
		Singleton.metalWindInstrument = new ArrayList <metalWindInstrument> ();
		Singleton.smartphones = new ArrayList <smartphones> ();
		Singleton.tvs = new ArrayList <tvs> ();
		String[] TipoDummies = { "SI","NO"};
		int op = 0;
		op = funciones_menu.menu("Cargar dummies", "Vols cargar dummies", TipoDummies);
			
		if (op==1)
			JOptionPane.showMessageDialog(null,"productos vacios", "No hay productos",JOptionPane.INFORMATION_MESSAGE);
			
		else{

			Dummies.DummiesInstruments();
			Dummies.DummiesPhones();
			Dummies.DummiesTvs();
		}
		
	}
	
	public static void cargarDummiesUsers(){
		//declarades public static en singleton
		Singleton.metalWindInstrument = new ArrayList <metalWindInstrument> ();
		Singleton.smartphones = new ArrayList <smartphones> ();
		Singleton.tvs = new ArrayList <tvs> ();
		
		Dummies.DummiesInstruments();
		Dummies.DummiesPhones();
		Dummies.DummiesTvs();
		
		
	}
}
