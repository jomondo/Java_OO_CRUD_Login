package Modules.SecondHandProducts.Model.CRUD;

import javax.swing.JOptionPane;
import Classes.Singleton;
import Modules.SecondHandProducts.Model.Classes.metalWindInstrument;
import Modules.SecondHandProducts.Model.Classes.smartphones;
import Modules.SecondHandProducts.Model.Classes.tvs;
import Modules.SecondHandProducts.Utils.funciones_products;

public class funciones_update {

	public static void UpdateMetalWindInstrument () {
		metalWindInstrument p = null;
		
		if(Singleton.metalWindInstrument.isEmpty()){
			JOptionPane.showMessageDialog(null, "Producto Vacio", "ERROR", JOptionPane.ERROR_MESSAGE);
			
		}else{
			int position = -1;
			p = funciones_find.CodMetalWindInstrument();
			if(p==null){
				JOptionPane.showMessageDialog(null, "El producto no existe", "ERROR", JOptionPane.ERROR_MESSAGE);	
			}else{
				position=funciones_find.FindMetalWindInstrument(p);
				if(position!=-1){
					p=Singleton.metalWindInstrument.get(position);
					funciones_products.UpdateProducts(p);
					Singleton.metalWindInstrument.set(position,p);
				}else{
					JOptionPane.showMessageDialog(null, "El producto no existe", "ERROR", JOptionPane.ERROR_MESSAGE);	
				}
					
			}
		}
	}
	
	public static void UpdateSmartphones () {
		smartphones p = null;
		
		if(Singleton.smartphones.isEmpty()){
			JOptionPane.showMessageDialog(null, "Producto Vacio", "ERROR", JOptionPane.ERROR_MESSAGE);
			
		}else{
			int position = -1;
			p = funciones_find.CodSmartphones();
			if(p==null){
				JOptionPane.showMessageDialog(null, "El producto no existe", "ERROR", JOptionPane.ERROR_MESSAGE);	
			}else{
				position=funciones_find.FindSmartphones(p);
				if(position!=-1){
					p=Singleton.smartphones.get(position);
					funciones_products.UpdateProducts(p);
					Singleton.smartphones.set(position,p);
				}else{
					JOptionPane.showMessageDialog(null, "El producto no existe", "ERROR", JOptionPane.ERROR_MESSAGE);	
				}
					
			}
		}
	}
	
	public static void UpdateTvs () {
		tvs p = null;
		
		if(Singleton.tvs.isEmpty()){
			JOptionPane.showMessageDialog(null, "Producto Vacio", "ERROR", JOptionPane.ERROR_MESSAGE);
			
		}else{
			int position = -1;
			p = funciones_find.CodTvs();
			if(p==null){
				JOptionPane.showMessageDialog(null, "El producto no existe", "ERROR", JOptionPane.ERROR_MESSAGE);
				
			}else{
				position=funciones_find.FindTvs(p);
				if(position!=-1){
					p=Singleton.tvs.get(position);
					funciones_products.UpdateProducts(p);
					Singleton.tvs.set(position,p);
				}else{
					JOptionPane.showMessageDialog(null, "El producto no existe", "ERROR", JOptionPane.ERROR_MESSAGE);	
				}
					
			}
		}
	}
	
	
}
