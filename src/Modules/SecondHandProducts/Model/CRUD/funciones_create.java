package Modules.SecondHandProducts.Model.CRUD;

import javax.swing.JOptionPane;

import Classes.Singleton;
import Modules.SecondHandProducts.Model.Classes.metalWindInstrument;
import Modules.SecondHandProducts.Model.Classes.smartphones;
import Modules.SecondHandProducts.Model.Classes.tvs;
import Modules.SecondHandProducts.Utils.funciones_products;


public class funciones_create {

	public static void CreateMetalWindInstrument () {
		int position = -1;
		metalWindInstrument p = null;
		
		p= funciones_products.pideMetalInstrumentCod();
		position = funciones_find.FindMetalWindInstrument(p);
		if (position != -1)
			JOptionPane.showMessageDialog(null, "No puedes crearlo porque este producto ya existe con ese mismo codigo", "ERROR", JOptionPane.ERROR_MESSAGE);
		
		else{
			p = (metalWindInstrument) funciones_products.CreateProducts(0);
			Singleton.metalWindInstrument.add(p);
			
		}
	}
	
	public static void CreateSmartphones () {
		int position = -1;
		smartphones p = null;
		
		p= funciones_products.pideSmartphonesCod();
		position = funciones_find.FindSmartphones(p);
		if (position != -1)
			JOptionPane.showMessageDialog(null, "No puedes crearlo porque este producto ya existe con ese mismo codigo", "ERROR", JOptionPane.ERROR_MESSAGE);
		
		else{
			p = (smartphones) funciones_products.CreateProducts(1);
			Singleton.smartphones.add(p);
			
		}
	}
	
	public static void CreateTvs () {
		int position = -1;
		tvs p = null;
		
		p= funciones_products.pideTvsCod();
		position = funciones_find.FindTvs(p);
		if (position != -1)
			JOptionPane.showMessageDialog(null, "No puedes crearlo porque este producto ya existe con ese mismo codigo", "ERROR", JOptionPane.ERROR_MESSAGE);
		
		else{
			p = (tvs) funciones_products.CreateProducts(2);
			Singleton.tvs.add(p);
			
		}
	}
}
