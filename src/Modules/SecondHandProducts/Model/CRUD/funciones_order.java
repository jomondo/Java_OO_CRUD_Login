package Modules.SecondHandProducts.Model.CRUD;

import java.util.Collections;

import javax.swing.JOptionPane;
import Classes.Singleton;
import Modules.SecondHandProducts.Model.Classes.Order.orderAnt;
import Modules.SecondHandProducts.Model.Classes.Order.orderBoquilla;
import Modules.SecondHandProducts.Model.Classes.Order.orderCapacity;
import Modules.SecondHandProducts.Model.Classes.Order.orderCompa�ia;
import Modules.SecondHandProducts.Model.Classes.Order.orderConexion;
import Modules.SecondHandProducts.Model.Classes.Order.orderConsumo;
import Modules.SecondHandProducts.Model.Classes.Order.orderFecha;
import Modules.SecondHandProducts.Model.Classes.Order.orderFmante;
import Modules.SecondHandProducts.Model.Classes.Order.orderMaterial;
import Modules.SecondHandProducts.Model.Classes.Order.orderName;
import Modules.SecondHandProducts.Model.Classes.Order.orderPulgadas;
import Modules.SecondHandProducts.Model.Classes.Order.orderTipo;
import Utils.funciones_menu;

public class funciones_order {

	public static void orderMetalWindInstrument () {
		int menu = 0;
		String [] optionsInstru = { "Codigo", "Antiguidad","Nombre","Fecha Fabricacion","Boquilla","Material","Fecha Mantenimiento" };
		
		if(Singleton.metalWindInstrument.isEmpty()){
			JOptionPane.showMessageDialog(null, "No hay productos", "ERROR", JOptionPane.ERROR_MESSAGE);
		}else{
			menu = funciones_menu.menu("�Como quieres ordenarlo?", "Ordenar", optionsInstru);
				switch (menu) {
					case 0:
						Collections.sort(Singleton.metalWindInstrument);
						break;
					case 1:
						Collections.sort(Singleton.metalWindInstrument, new orderAnt());
						break;
					case 2:
						Collections.sort(Singleton.metalWindInstrument, new orderName());
						break;
					case 3:
						Collections.sort(Singleton.metalWindInstrument, new orderFecha());
						break;
					case 4:
						Collections.sort(Singleton.metalWindInstrument, new orderBoquilla());
						break;
					case 5:
						Collections.sort(Singleton.metalWindInstrument, new orderMaterial());
						break;
					case 6:
						Collections.sort(Singleton.metalWindInstrument, new orderFmante());
						break;
				}
		}
	}
	
	
	public static void orderSmartphones () {
		int menu = 0;
		String [] optionsPhones = { "Codigo", "Antiguidad","Nombre","Fecha Fabricacion","Pulgadas","Compa�ia","Capacidad" };
		
		if(Singleton.smartphones.isEmpty()){
			JOptionPane.showMessageDialog(null, "No hay productos", "ERROR", JOptionPane.ERROR_MESSAGE);
		}else{
			menu = funciones_menu.menu("�Como quieres ordenarlo?", "Ordenar", optionsPhones);
				switch (menu) {
					case 0:
						Collections.sort(Singleton.smartphones);
						break;
					case 1:
						Collections.sort(Singleton.smartphones, new orderAnt());
						break;
					case 2:
						Collections.sort(Singleton.smartphones, new orderName());
						break;
					case 3:
						Collections.sort(Singleton.smartphones, new orderFecha());
						break;
					case 4:
						Collections.sort(Singleton.smartphones, new orderPulgadas());
						break;
					case 5:
						Collections.sort(Singleton.smartphones, new orderCompa�ia());
						break;
					case 6:
						Collections.sort(Singleton.smartphones, new orderCapacity());
						break;
				}
		}
	}
	
	
	public static void orderTvs () {
		int menu = 0;
		String [] optionsTvs = { "Codigo", "Antiguidad","Nombre","Fecha Fabricacion","Consumo","Tipo","Conexion" };
		
		if(Singleton.tvs.isEmpty()){
			JOptionPane.showMessageDialog(null, "No hay productos", "ERROR", JOptionPane.ERROR_MESSAGE);
		}else{
			menu = funciones_menu.menu("�Como quieres ordenarlo?", "Ordenar", optionsTvs);
				switch (menu) {
					case 0:
						Collections.sort(Singleton.tvs);
						break;
					case 1:
						Collections.sort(Singleton.tvs, new orderAnt());
						break;
					case 2:
						Collections.sort(Singleton.tvs, new orderName());
						break;
					case 3:
						Collections.sort(Singleton.tvs, new orderFecha());
						break;
					case 4:
						Collections.sort(Singleton.tvs, new orderConsumo());
						break;
					case 5:
						Collections.sort(Singleton.tvs, new orderTipo());
						break;
					case 6:
						Collections.sort(Singleton.tvs, new orderConexion());
						break;
				}
		}
	}
	
	
}
