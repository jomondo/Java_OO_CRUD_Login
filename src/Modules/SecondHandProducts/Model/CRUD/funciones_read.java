package Modules.SecondHandProducts.Model.CRUD;

import javax.swing.JOptionPane;
import Classes.Singleton;
import Modules.SecondHandProducts.Model.Classes.metalWindInstrument;
import Modules.SecondHandProducts.Model.Classes.smartphones;
import Modules.SecondHandProducts.Model.Classes.tvs;

public class funciones_read {

	public static void ReadMetalWindInstruments (){
		
		if(Singleton.metalWindInstrument.isEmpty()){
			JOptionPane.showMessageDialog(null, "No hay productos", "ERROR", JOptionPane.ERROR_MESSAGE);
		}else{
			for (int i = 0; i <Singleton.metalWindInstrument.size();i++){
				String cad = "";
				cad = cad + (Singleton.metalWindInstrument.get(i).toString());
				JOptionPane.showMessageDialog(null, cad);
			}
		}
	}
	
	public static void ReadSmartphones (){	
		if(Singleton.smartphones.isEmpty()){
			JOptionPane.showMessageDialog(null, "No hay productos", "ERROR", JOptionPane.ERROR_MESSAGE);
		}else{
			for (int i = 0; i <Singleton.smartphones.size();i++){
				String cad = "";
				cad = cad + (Singleton.smartphones.get(i).toString());
				JOptionPane.showMessageDialog(null, cad);
			}
		}
	}
	
	public static void ReadTvs (){	
		if(Singleton.tvs.isEmpty()){
			JOptionPane.showMessageDialog(null, "No hay productos", "ERROR", JOptionPane.ERROR_MESSAGE);
		}else{
			for (int i = 0; i <Singleton.tvs.size();i++){
				String cad = "";
				cad = cad + (Singleton.tvs.get(i).toString());
				JOptionPane.showMessageDialog(null, cad);
			}
		}
	}
	
	
	public static void ReadMetalWindInstrument (){
		metalWindInstrument p = null;
		
		if(Singleton.metalWindInstrument.isEmpty()){
			JOptionPane.showMessageDialog(null, "No hay productos", "ERROR", JOptionPane.ERROR_MESSAGE);
			
		}else{
			int position=-1;
			p=funciones_find.CodMetalWindInstrument();
			if(p==null){
				JOptionPane.showMessageDialog(null, "No existe el producto", "ERROR", JOptionPane.ERROR_MESSAGE);
			}else{
				position = funciones_find.FindMetalWindInstrument(p);
				if(position!=-1){
					p=Singleton.metalWindInstrument.get(position);
					JOptionPane.showMessageDialog(null, p.toString());
				}else{
					JOptionPane.showMessageDialog(null, "No existe el producto", "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
			
		}
	}
	
	public static void ReadSmartphone (){
		smartphones p = null;
		
		if(Singleton.smartphones.isEmpty()){
			JOptionPane.showMessageDialog(null, "No hay productos", "ERROR", JOptionPane.ERROR_MESSAGE);
			
		}else{
			int position=-1;
			p=funciones_find.CodSmartphones();
			if(p==null){
				JOptionPane.showMessageDialog(null, "No existe el producto", "ERROR", JOptionPane.ERROR_MESSAGE);
			}else{
				position = funciones_find.FindSmartphones(p);
				if(position!=-1){
					p=Singleton.smartphones.get(position);
					JOptionPane.showMessageDialog(null, p.toString());
				}else{
					JOptionPane.showMessageDialog(null, "No existe el producto", "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
			
		}
	}
	
	public static void ReadTv (){
		tvs p = null;
		
		if(Singleton.tvs.isEmpty()){
			JOptionPane.showMessageDialog(null, "No hay productos", "ERROR", JOptionPane.ERROR_MESSAGE);
			
		}else{
			int position=-1;
			p=funciones_find.CodTvs();
			if(p==null){
				JOptionPane.showMessageDialog(null, "No existe el producto", "ERROR", JOptionPane.ERROR_MESSAGE);
			}else{
				position = funciones_find.FindTvs(p);
				if(position!=-1){
					p=Singleton.tvs.get(position);
					JOptionPane.showMessageDialog(null, p.toString());
				}else{
					JOptionPane.showMessageDialog(null, "No existe el producto", "ERROR", JOptionPane.ERROR_MESSAGE);
				}
			}
			
		}
	}
}
