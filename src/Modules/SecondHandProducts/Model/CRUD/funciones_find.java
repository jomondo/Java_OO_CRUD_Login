package Modules.SecondHandProducts.Model.CRUD;

import javax.swing.JOptionPane;
import Classes.Singleton;
import Modules.SecondHandProducts.Model.Classes.metalWindInstrument;
import Modules.SecondHandProducts.Model.Classes.smartphones;
import Modules.SecondHandProducts.Model.Classes.tvs;
import Utils.funciones_menu;

public class funciones_find {

	public static int FindMetalWindInstrument(metalWindInstrument metalWindInstrument) {
		for (int i = 0; i<=(Singleton.metalWindInstrument.size()-1); i++){
			if((Singleton.metalWindInstrument.get(i)).equals(metalWindInstrument) )
				return i;
		}
		return -1;
	}
	
	public static int FindSmartphones(smartphones smartphones) { 
		for (int i = 0; i<=(Singleton.smartphones.size()-1); i++){
			if((Singleton.smartphones.get(i)).equals(smartphones) )
				return i;
		}
		return -1;
	}
	
	public static int FindTvs(tvs tvs) { 
		for (int i = 0; i<=(Singleton.tvs.size()-1); i++){
			if((Singleton.tvs.get(i)).equals(tvs) )
				return i;
		}
		return -1;
	}
	
	public static metalWindInstrument CodMetalWindInstrument () {
		metalWindInstrument p = null;
		String ID = "";
		boolean rdo=false;
		
		do{
			String [] metalWindInstrument = GenerateMetalWindInstrument ();
			String search = funciones_menu.combobox("Selecciona el producto", "�Que deseas?", metalWindInstrument);
			System.out.println("Search: "+search);
			
			if (search != ""){
				for (int i = 0; i<2; i++) {//pillem els dos primers caracters que son el que formen el codic del prod
					ID += search.charAt(i);
				}
				System.out.println("ID: " + ID);
				p = new metalWindInstrument (ID);
				rdo=true;
			}else{
				JOptionPane.showMessageDialog(null, "Tienes que seleccionar un producto");
				rdo=false;
			}
			
		}while(rdo==false);
		
		return p;		
	}
	public static smartphones CodSmartphones () {
		smartphones p = null;
		String ID = "";
		boolean rdo=false;
		
		do{
			String [] smartphones = GenerateSmartphones ();
			String search = funciones_menu.combobox("Selecciona el producto", "�Que deseas?", smartphones);
			System.out.println("Search: "+search);
			
			if (search != ""){
				for (int i = 0; i<2; i++) {//pillem els dos primers caracters que son el que formen el codic del prod
					ID += search.charAt(i);
				}
				System.out.println("ID: " + ID);
				p = new smartphones (ID);
				rdo=true;
			}else{
				JOptionPane.showMessageDialog(null, "Tienes que seleccionar un producto");
				rdo=false;
			}
		}while(rdo==false);
		
		return p;		
	}
	
	public static tvs CodTvs () {
		boolean rdo = false;
		tvs p = null;
		String ID = "";
		
		String [] tvs = GenerateTvs ();
		//fins que el usuari no el.legeix un producte no el deixarem passar
		do{
			String search = funciones_menu.combobox("Selecciona el producto", "�Que deseas?", tvs);
			System.out.println("Search: "+search);
		
			if (search != ""){
				for (int i = 0; i<2; i++) {//pillem els dos primers caracters que son el que formen el codic del prod
					ID += search.charAt(i);
				}
				System.out.println("ID: " + ID);
				p = new tvs (ID);
				rdo=true;
			}else{
				JOptionPane.showMessageDialog(null, "Tienes que seleccionar un producto");
				rdo=false;
			}
		}while(rdo==false);
		
		return p;		
	}
	
	
	public static String[] GenerateMetalWindInstrument () {
		metalWindInstrument p = null;
		String s = "";
		int i = 0;
		
		int arraylist =Singleton.metalWindInstrument.size();
		String [] prod = new String[arraylist];
		for ( i = 0; i<arraylist; i++) {//Generem el arraylist en un combobox
			p = (metalWindInstrument) Singleton.metalWindInstrument.get(i);//i sera la posicio de cada element
			s=p.getCod()+" ----- "+p.getNombre()+" ----- "+p.getPrecio();
			prod[i] = s;
			
		}
		return prod;	
	}
	
	public static String[] GenerateSmartphones () {
		smartphones p = null;
		String s = "";
		int i = 0;
		
		int arraylist =Singleton.smartphones.size();
		String [] prod = new String[arraylist];
		for ( i = 0; i<arraylist; i++) {//Generem el arraylist en un combobox
			p = (smartphones) Singleton.smartphones.get(i);
			s=p.getCod()+" ----- "+p.getNombre()+" ----- "+p.getPrecio();
			prod[i] = s;
			
		}
		return prod;	
	}
	
	public static String[] GenerateTvs () {
		tvs p = null;
		String s = "";
		int i = 0;
		
		int arraylist =Singleton.tvs.size();
		String [] prod = new String[arraylist];
		for ( i = 0; i<arraylist; i++) {//Generem el arraylist en un combobox
			p = (tvs) Singleton.tvs.get(i);//i sera la posicio de cada element
			s=p.getCod()+" ----- "+p.getNombre()+" ----- "+p.getPrecio();
			prod[i] = s;
			
		}
		return prod;	
	}
}
