package Modules.SecondHandProducts.Model.CRUD;

import javax.swing.JOptionPane;
import Classes.Singleton;
import Modules.SecondHandProducts.Model.Classes.metalWindInstrument;
import Modules.SecondHandProducts.Model.Classes.smartphones;
import Modules.SecondHandProducts.Model.Classes.tvs;

public class funciones_delete {

	public static void DeleteMetalWindInstrument () {
		metalWindInstrument p = null;
		
		if(Singleton.metalWindInstrument.isEmpty()){
			JOptionPane.showMessageDialog(null, "No hay productos", "ERROR", JOptionPane.ERROR_MESSAGE);
			
		}else{
			int position=-1;
			p=funciones_find.CodMetalWindInstrument();
			if(p==null){
				JOptionPane.showMessageDialog(null, "No existe este producto", "ERROR", JOptionPane.ERROR_MESSAGE);
			}else{
				position=funciones_find.FindMetalWindInstrument(p);
				if(position!=-1){
					Singleton.metalWindInstrument.remove(position);
					JOptionPane.showMessageDialog(null, "Borrado");
				}else{
					JOptionPane.showMessageDialog(null, "No se ha podido borrar","ERROR", JOptionPane.ERROR_MESSAGE);
				}
					
			}
		}
	}
	
	public static void DeleteSmartphones () {
		smartphones p = null;
		
		if(Singleton.smartphones.isEmpty()){
			JOptionPane.showMessageDialog(null, "No hay productos", "ERROR", JOptionPane.ERROR_MESSAGE);
			
		}else{
			int position=-1;
			p=funciones_find.CodSmartphones();
			if(p==null){
				JOptionPane.showMessageDialog(null, "No existe este producto", "ERROR", JOptionPane.ERROR_MESSAGE);
			}else{
				position=funciones_find.FindSmartphones(p);
				if(position!=-1){
					Singleton.smartphones.remove(position);
					JOptionPane.showMessageDialog(null, "Borrado");
				}else{
					JOptionPane.showMessageDialog(null, "No se ha podido borrar","ERROR", JOptionPane.ERROR_MESSAGE);
				}		
			}
		}
	}
	
	public static void DeleteTvs () {
		tvs p = null;
		
		if(Singleton.tvs.isEmpty()){
			JOptionPane.showMessageDialog(null, "No hay productos", "ERROR", JOptionPane.ERROR_MESSAGE);
			
		}else{
			int position=-1;
			p=funciones_find.CodTvs();
			if(p==null){
				JOptionPane.showMessageDialog(null, "No existe este producto", "ERROR", JOptionPane.ERROR_MESSAGE);
				
			}else{
				position=funciones_find.FindTvs(p);
				if(position!=-1){
					Singleton.tvs.remove(position);
					JOptionPane.showMessageDialog(null, "Borrado");
					
				}else{
					JOptionPane.showMessageDialog(null, "No se ha podido borrar","ERROR", JOptionPane.ERROR_MESSAGE);
					
				}		
			}
		}
	}
}
