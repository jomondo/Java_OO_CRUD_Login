package Modules.SecondHandProducts.Model.Graphs.Utils;

import Classes.Singleton;
import Modules.SecondHandProducts.Model.Classes.metalWindInstrument;
import Modules.SecondHandProducts.Model.Classes.secondHandProducts;
import Modules.SecondHandProducts.Model.Classes.smartphones;
import Modules.SecondHandProducts.Model.Classes.tvs;

public class funciones_Statistic {
	
	public static float priceTotalMetalWindInstrument (){
		int arraylistI =Singleton.metalWindInstrument.size();
		float s = 0.0f;
		secondHandProducts p = null;
		float sumI = 0.0f;
		
		for (int i = 0; i<arraylistI; i++) {
			p = (metalWindInstrument) Singleton.metalWindInstrument.get(i);
			s=p.getPrecio();
			sumI = sumI + s;
			
		}
		return sumI;
	}
	
	public static float priceTotalSmartphones (){
		float s = 0.0f;
		secondHandProducts p = null;
		float sumS = 0.0f;
		int arraylistS =Singleton.smartphones.size();
		for (int i = 0; i<arraylistS; i++) {
			p = (smartphones) Singleton.smartphones.get(i);
			s=p.getPrecio();
			sumS = sumS + s;
			
		}
		return sumS;
	}
	
	public static float priceTotalTvs (){
		int arraylistT =Singleton.tvs.size();
		float s = 0.0f;
		secondHandProducts p = null;
		float sumT = 0.0f;

		for (int i = 0; i<arraylistT; i++) {
			p = (tvs) Singleton.tvs.get(i);
			s=p.getPrecio();
			sumT = sumT + s;
			
		}
		return sumT;
	}

}
