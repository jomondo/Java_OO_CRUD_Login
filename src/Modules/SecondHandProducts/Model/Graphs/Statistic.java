package Modules.SecondHandProducts.Model.Graphs;
import java.awt.FlowLayout;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import Classes.Singleton;
import Modules.SecondHandProducts.Model.Graphs.Utils.funciones_Statistic;

public class Statistic {
	
	public Statistic(){
		float sumI = 0.0f, sumS=0.0f, sumT=0.0f;
		
		if(Singleton.metalWindInstrument.isEmpty()||Singleton.smartphones.isEmpty()||Singleton.tvs.isEmpty()){
			JOptionPane.showMessageDialog(null,"Tiene que haber un producto como minimo en cada uno de ellos","No hay productos en todos ellos", JOptionPane.ERROR_MESSAGE);
		}else{
			DefaultCategoryDataset data = new DefaultCategoryDataset();
			final String Product1="Instruments";
			final String Product2="Smartphones";
			final String Product3="Tvs";

			sumI = funciones_Statistic.priceTotalMetalWindInstrument();
			sumS = funciones_Statistic.priceTotalSmartphones();
			sumT = funciones_Statistic.priceTotalTvs();
			
			data.addValue(sumI,Product1 , "Second Hand Products");
			data.addValue(sumS,Product2 , "Second Hand Products");
			data.addValue(sumT,Product3 , "Second Hand Products");
			
			JFreeChart graph = ChartFactory.createBarChart3D("Second Hand Products", "Products", "Value Price", data, PlotOrientation.VERTICAL, true, true, false);
			
			ChartPanel contenedor=new ChartPanel(graph);
			contenedor.setVisible(true);
			JFrame ventana = new JFrame("Grafics Products");
			ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			FlowLayout layout = new FlowLayout(FlowLayout.CENTER, 15, 15);//coloca en fila
		    ventana.setLayout(layout);
			ventana.add(contenedor);
			ventana.setSize(800, 500);
			ventana.setVisible(true);
			ventana.setLocation(565, 0);
			ventana.setResizable(false);
			//millorar
			JOptionPane.showMessageDialog(null,"Salir de aqui?", "SALIR",JOptionPane.QUESTION_MESSAGE);//de moment a l'unica manera que no aparega el menu de seguida
			ventana.dispose();
			
		}
		
	}
	
}
