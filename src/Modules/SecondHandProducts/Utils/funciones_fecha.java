package Modules.SecondHandProducts.Utils;

import javax.swing.JOptionPane;
import Classes.Fecha;
import Utils.funciones;
import Utils.validate_products;
public class funciones_fecha {

	public static Fecha PideFechaFabricacion (){	
		int ant = 0;
		boolean result = true;
		Fecha Fcompra = null;
		
		do{
			String startDate = funciones.ValidaString("Introduce", "Escribe la fecha de fabricacion\nEj: 27/04/2000");
			System.out.println("Fecha introducida: " + startDate);
			boolean rdo1=validate_products.ValidaFetxa(startDate);
			Fcompra = new Fecha (startDate);
			boolean rdo2=Fcompra.checkDates();
			
			if((rdo1 && rdo2)==true){	
				System.out.println("Fecha validada");
				ant = Fcompra.subtractDates();
				
				if((ant<0)||(ant>55)){	
					System.out.println("Este producto tiene mas de 55 a�os y no lo queremos");
					JOptionPane.showMessageDialog(null,"Producto con mas de 55 a�os", "ERROR",JOptionPane.ERROR_MESSAGE);
					result=false;
					
				}else	
					result=true;
				
			}else{
				System.out.println("Fecha incorrecta");
				JOptionPane.showMessageDialog(null,"Fecha incorrecta (dd/mm/aaaa)", "ERROR",JOptionPane.ERROR_MESSAGE);
				result=false;
				
			}	
		}while (result==false);
		
		return Fcompra;
		
	}
	
	public static Fecha PideFechActualizacion (Fecha Ffabric){//smartphone
	int rdo = 0;
	boolean result = true;
	Fecha Fupdate = null;
	int antiguitat = 0;
		
	do{	
		String fupdate = funciones.ValidaString("Introduce", "Escribe fecha ultima actulizacion software\n Ej:(dd/mm/aaaa)");
		result = validate_products.ValidaFetxa(fupdate);
			
		if(result == false){
			System.out.println("Formato fecha no valida");
			JOptionPane.showMessageDialog(null,"�Seguro? Vuelve a intentarlo", "Formato no valido",JOptionPane.ERROR_MESSAGE);
			
		}else{
			Fupdate = new Fecha (fupdate);
			result = Fupdate.checkDates();
			System.out.println("Fecha introducida: " + Fupdate.ToString());
			
			if(result==false){
				System.out.println("La fecha no es valida");
				JOptionPane.showMessageDialog(null,"�Estas seguro que esa fecha existe?", "Fecha no valida",JOptionPane.ERROR_MESSAGE);
				
			}else{
				rdo = Fupdate.compareDate(Ffabric);
				System.out.println("Compara fechas: " + rdo);//if rdo==2, it's correct
						
					if(rdo==-1){	
						JOptionPane.showMessageDialog(null,"No puede ser, aun no estaba en existencia!", "Fecha anterior",JOptionPane.ERROR_MESSAGE);
						result = false;
					}else{	
						antiguitat = Fupdate.subtractDates();
						
						if(antiguitat>5){
							JOptionPane.showMessageDialog(null,"No queremos smartphones con mas de 5 a�os el mismo software", "Desactualizado",JOptionPane.ERROR_MESSAGE);
							System.out.println("Antiguedad menor que 0");
							result = false;
							
						}	
					}
				}
		}		 	
	}while(result==false);
		return Fupdate;
	}
	
	public static Fecha PideFechMantenimiento (Fecha Ffabric){//Instruments
		int rdo = 0;
		boolean result = true;
		Fecha FMante = null;
		int antiguitat = 0;
			
		do{	
			String fmante = funciones.ValidaString("Introduce", "Escribe ultima fecha de mantenimiento\n Ej:(dd/mm/aaaa)");
			result = validate_products.ValidaFetxa(fmante);
				
			if(result == false){
				System.out.println("Formato fecha no valida");
				JOptionPane.showMessageDialog(null,"�Seguro? Vuelve a intentarlo", "Formato no valido",JOptionPane.ERROR_MESSAGE);
				
			}else{
				FMante = new Fecha (fmante);
				result = FMante.checkDates();
				System.out.println("Fecha introducida: " + FMante.ToString());
				
				if(result==false){
					System.out.println("La fecha no es valida");
					JOptionPane.showMessageDialog(null,"�Estas seguro que esa fecha existe?", "Fecha no valida",JOptionPane.ERROR_MESSAGE);
					
				}else{
					rdo = FMante.compareDate(Ffabric);
					System.out.println("Compara fechas: " + rdo);//if rdo==2, it's correct
							
						if(rdo==-1){	
							JOptionPane.showMessageDialog(null,"No puede ser, aun no estaba en existencia!", "Fecha anterior",JOptionPane.ERROR_MESSAGE);
							result = false;
						}else{	
							antiguitat = FMante.subtractDates();
							//int ant = Ffabric.subtractDates();
							//result = funciones_fecha.comprovaMantenimiento(ant, antiguitat);//a tantos a�os de anti tubo la ultima actualizacion
							
							if(antiguitat>3){
								JOptionPane.showMessageDialog(null,"No queremos instrumentos con mas de 3 a�os sin revision alguna", "Revision mas de 3 a�os",JOptionPane.ERROR_MESSAGE);
								System.out.println("Antiguedad mayor que 3");
								result = false;
								
							}	
						}
					}
			}		 	
		}while(result==false);
			return FMante;
		}
	
	
	
	public static boolean comprovaMantenimiento (int n1, int n2){
		boolean result;
		if((n1 - n2)>=16)
			result = true;
	
		else
			result = false;
		
		return result;
		
	}
}
