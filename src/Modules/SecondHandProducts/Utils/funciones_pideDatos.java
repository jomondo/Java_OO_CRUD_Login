package Modules.SecondHandProducts.Utils;
import Utils.funciones;
import Utils.funciones_menu;
import Utils.validate_products;


public class funciones_pideDatos {

	public static String nombre (){
		String nom = "";
		boolean result = true;
		
		do{
			nom = funciones.ValidaString("Introduce","Escribe el nombre");
			result = validate_products.ValidaNom(nom);
			
		}while(result == false);
		
		return nom;
	}
	
	public static String cod (){
		String nom = "";
		boolean result = true;
		
		do{
			nom = funciones.ValidaString("Introduce","Escribe el codigo\nEj:F6");
			result = validate_products.ValidaCod(nom);
			
		}while(result == false);
		
		return nom;
	}
	
	public static String marca (int i){
		
		String material = "";
		if(i==0){
			String[] tipos = { "Bach", "Yamaha", "B&S", "Schilke", "Thomann", "Adams", "CarolBrass", "Gerd Dowids", "Johannes Scherzer","Stone"};
			boolean result = true;
			
			do{
				material = funciones_menu.combobox("Elije", "�De que marca es", tipos);
				if(material=="")
					result = false;
					
				else{
					System.out.println("Marca Instru: "+material.toString());
					result=true;
				}
			}while(result==false);	
			
		}
		if(i==2){
			String[] tipos = { "LG", "Samsung", "Sony", "JVC", "Sanyo", "Philips", "Sharp", "Technics", "Benq",
		            "Panasonic", "Airis", "Toshiba", "OKI", "Thomson"};
			boolean result = true;
			
			
			do{
				material = funciones_menu.combobox("Elije", "�De que marca es?", tipos);
				if(material=="")
					result = false;
					
				else{
					System.out.println("Marca TV: "+material.toString());
					result=true;
				}
			}while(result==false);	
			
		}if(i==1){
			String[] tipos = {"LG", "Samsung", "Asus", "Xiaomi", "BQ", "Dogee", "Motorola", "Huawei", "Sony Ericsson",
		            "Nokia", "BlackBerry", "Alcatel", "HTC", "iphone"};
			boolean result = true;
			
			do{
				material = funciones_menu.combobox("Elije", "�De que marca es?", tipos);
				if(material=="")
					result = false;
					
				else{
					System.out.println("Marca phone: "+material.toString());
					result=true;
				}
			}while(result==false);	
		}
		return material;	
	}
	
	public static String color (){
		String nom = "";
		String[] tipos = { "Negro", "Blanco", "Rojo", "Amarillo", "Azul", "Verde", "Marron", "Violeta", "Gris",
	            "Cian", "Magenta", "Escarlata", "Carmesi", "Bermellon", "Granate", "Carmin", "Amaranto", "Pistacho", "Verde Oliva",
	            "Xanadu", "Plata", "Plata metalizado", "Zafiro"};
		boolean result = true;
		
		do{
			nom = funciones_menu.combobox("Elije", "�De que color es?", tipos);
			if(nom=="")
				result = false;
				
			else
				result=true;
			
		}while(result==false);	
		
		return nom;
	}
	
	public static float precio (){
		float precio = 0.0f;
		boolean result = true;
		
		do{
			precio = funciones.ValidaFloat("Introduce","Escribe un precio");
			String p = Float.toString(precio);
			result = validate_products.ValidaPrecio(p);	
			
		}while(result == false);
		
		return precio;
	}
	
	public static String DNI (){
		String DNI = "";
		boolean result = true;
		
		do{
			DNI = funciones.ValidaString("Introduce","Escribe el DNI del antiguo propietario");
			result = validate_products.ValidaDni(DNI);	
			
		}while(result == false);
		
		return DNI;
	}
	public static String material (){
		String[] tipos = { "laton","ba�o de plata","plata","platino", "oro", "oro 14kt", "oro 9kt"};
		boolean result = true;
		String material = "";
		
		do{
			material = funciones_menu.combobox("Elije", "�De que tipo material esta hecho?", tipos);
			if(material=="")
				result = false;
				
			else
				result=true;
			
		}while(result==false);	
		
		return material;
	}
	
	public static String afinacion (){
		String[] tipos = { "do","sib","mib","sol","fa","re","lab"};
		boolean result = true;
		String afi = "";
		
		do{
			afi = funciones_menu.combobox("Elije", "�En que nota esta afinado el instrumento?", tipos);
			if(afi=="")
				result = false;
			else
				result=true;
			
		}while(result==false);		

		return afi;
	}
	
	public static String boquilla (){
		String boquilla = "";
		boolean result = true;
		
		do{
			boquilla = funciones.ValidaString("Introduce","Escribe tipo boquilla\nEj: A2");
			result = validate_products.ValidaBoquilla(boquilla);	
			
		}while(result == false);
		
		return boquilla;
	}
	
	public static float campana (){
		String campana = "";
		boolean result = true;
		
		do{
			campana = funciones.ValidaString("Introduce","Escribe tama�o campana\nEj: 123.12 mm");
			result = validate_products.ValidaCampana(campana);	
			
		}while(result == false);
		float cam = Float.parseFloat(campana);
		return cam;
	}
	
	public static String calibre (){
		String[] tipos = { "bajo","medio-bajo","medio","medio-alto","alto"};
		boolean result = true;
		String calibre = "";
		
		do{
			calibre = funciones_menu.combobox("Elije", "�De que tipo calibre es?", tipos);
			if(calibre=="")
				result = false;
			
			else
				result=true;
			
		}while(result==false);
		
				

		return calibre;
	}
	
	public static String funda (){
		String[] tipos = { "Si","No" };
		boolean result = true;
		String funda = "";
		
		do{
			funda = funciones_menu.combobox("Elije", "�Lleva funda?", tipos);
			if(funda=="")
				result = false;
			
			else
				result=true;
			
		}while(result==false);		

		return funda;
	}
	
	public static int pulgadas (){
		String pulg = "";
		boolean result = true;
		
		do{
			pulg = funciones.ValidaString("Introduce","Escribe tama�o en pulgadas\nEj: 12 ");
			result = validate_products.ValidaPulgadas(pulg);	
			
		}while(result == false);
		int pulgadas = Integer.parseInt(pulg);
		return pulgadas;
	}
	
	public static int duracionBat (){
		String bat = "";
		boolean result = true;
		
		do{
			bat = funciones.ValidaString("Introduce","Escribe la duracion max en horas\nEj: 3 o 10 ");
			result = validate_products.ValidaBateria(bat);	
			
		}while(result == false);
		int bateria = Integer.parseInt(bat);
		return bateria;
	}
	public static int pixelesCamara (){
		String pixel = "";
		boolean result = true;
		
		do{
			pixel = funciones.ValidaString("Introduce","Escribe pixeles de la camara\nEj: 3 o 10 ");
			result = validate_products.ValidaBateria(pixel);//la mateixa	
			
		}while(result == false);
		int camerapix = Integer.parseInt(pixel);
		return camerapix;
	}
	
	public static String compa�ia (){
		String[] tipos = { "Movistar","Vodafone","Yoigo", "Amena", "Orange","Jazztel", "Lowi", "Ono","MasMovil","Quantis", "Tuenti","LIBRE" };
		boolean result = true;
		String compa�ia = "";
		
		do{
			compa�ia = funciones_menu.combobox("�De que compa�ia es?", "Elije", tipos);
			if(compa�ia=="")
				result = false;
			
			else
				result=true;
			
		}while(result==false);		

		return compa�ia;
	}
	
	public static String procesador (){
		String[] tipos = { "Qualcomm Snapdragon 835","Snapdragon 820","Leadcore", "Apple A10", "Apple A9","Kirin 955", "MTK Helio X20", "Samsung Exynos 7420","Otro" };
		boolean result = true;
		String cpu = "";
		
		do{
			cpu = funciones_menu.combobox("�Que procesador lleva?", "Elije", tipos);
			if(cpu=="")
				result = false;
			
			else
				result=true;
			
		}while(result==false);		

		return cpu;
	}
	
	public static int ram (){
		String ram = "";
		boolean result = true;
		
		do{
			ram = funciones.ValidaString("Introduce","Escribe los gb de la RAM\nEj: 3 o 10 ");
			result = validate_products.ValidaBateria(ram);//la mateixa	
			
		}while(result == false);
		int Ram = Integer.parseInt(ram);
		return Ram;
	}
	
	public static String sistema (){
		String[] tipos = { "Android","Windows Phone","iOS", "BlackBerry OS", "Otro" };
		boolean result = true;
		String sistema = "";
		
		do{
			sistema = funciones_menu.combobox("�Que sistema operativo lleva?", "Elije", tipos);
			if(sistema=="")
				result = false;
			
			else
				result=true;
			
		}while(result==false);		

		return sistema;
	}
	
	public static int capacidad (){
		String capacidad = "";
		boolean result = true;
		
		do{
			capacidad = funciones.ValidaString("Introduce","Escribe la capacidad interna(gb)\nEj: 3 o 10 o 100 ");
			result = validate_products.ValidaCapacidad(capacidad);//la mateixa	
			
		}while(result == false);
		int Cap = Integer.parseInt(capacidad);
		return Cap;
	}
	
	public static String hd (){
		String[] tipos = { "Si","No" };
		boolean result = true;
		String hd = "";
		
		do{
			hd = funciones_menu.combobox("�Es full HD?", "Elije", tipos);
			if(hd=="")
				result = false;
			
			else
				result=true;
			
		}while(result==false);		

		return hd;
	}
	public static String led (){
		String[] tipos = { "Si","No" };
		boolean result = true;
		String led = "";
		
		do{
			led = funciones_menu.combobox("�Es led?", "Elije", tipos);
			if(led=="")
				result = false;
			
			else
				result=true;
			
		}while(result==false);		

		return led;
	}
	public static String ahorroInteligente (){
		String[] tipos = { "Si","No" };
		boolean result = true;
		String ahorro = "";
		
		do{
			ahorro = funciones_menu.combobox("�Tiene ahorro inteligente?", "Elije", tipos);
			if(ahorro=="")
				result = false;
			
			else
				result=true;
			
		}while(result==false);		

		return ahorro;
	}
	
	public static String tipoTv (){

		String[] tipos = { "Smart TV","4K/UHD","Curvos","QLED" };
		boolean result = true;
		String tipoTv = "";
		
		do{
			tipoTv = funciones_menu.combobox("�Que tipo TV es?", "Elije", tipos);
			if(tipoTv=="")
				result = false;
			
			else
				result=true;
			
		}while(result==false);		

		return tipoTv;
	}
	
	public static int consumo (){
		String consumo = "";
		boolean result = true;
		
		do{
			consumo = funciones.ValidaString("Introduce","Escribe el consumo electrico\nEj: 3 o 10 o 100 ");
			result = validate_products.ValidaConsumo(consumo);//la mateixa	
			
		}while(result == false);
		int Con = Integer.parseInt(consumo);
		return Con;
	}
	
	public static String conexion (){

		String[] tipos = { "Wifi","Ethernet","Wifi y Ethernet","No tiene" };
		boolean result = true;
		String conexion = "";
		
		do{
			conexion = funciones_menu.combobox("�Que tipo conexion tiene?", "Elije", tipos);
			if(conexion=="")
				result = false;
			
			else
				result=true;
			
		}while(result==false);		

		return conexion;
	}
}
