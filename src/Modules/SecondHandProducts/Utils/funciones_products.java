package Modules.SecondHandProducts.Utils;
import javax.swing.JOptionPane;
import Classes.Fecha;
import Classes.Singleton;
import Modules.SecondHandProducts.Model.Classes.metalWindInstrument;
import Modules.SecondHandProducts.Model.Classes.secondHandProducts;
import Modules.SecondHandProducts.Model.Classes.smartphones;
import Modules.SecondHandProducts.Model.Classes.tvs;
import Modules.SecondHandProducts.Model.CRUD.funciones_create;
import Utils.funciones_menu;
import Modules.SecondHandProducts.Model.CRUD.*;

public class funciones_products {

	public static secondHandProducts CreateProducts (int op){
		secondHandProducts u = null;
		
		if(op == 0){
			
			String cod = Singleton.cod;
			String nom = funciones_pideDatos.nombre();
			Fecha fecha = funciones_fecha.PideFechaFabricacion();
			Fecha Fmante = funciones_fecha.PideFechMantenimiento(fecha);
			String marca = funciones_pideDatos.marca(0);
			String color = funciones_pideDatos.color();
			float precio = funciones_pideDatos.precio();
			String DNI = funciones_pideDatos.DNI();
			String material = funciones_pideDatos.material();
			String afinacion = funciones_pideDatos.afinacion();
			String boquilla = funciones_pideDatos.boquilla();
			float campana = funciones_pideDatos.campana();
			String calibre = funciones_pideDatos.calibre();
			String funda = funciones_pideDatos.funda();
			
			u = new metalWindInstrument (cod,nom,fecha,Fmante,marca,color,DNI,precio,material, afinacion,boquilla, campana, calibre, funda);
			
		}
		
		if(op == 1){
	
			String cod = Singleton.cod;
			String nom = funciones_pideDatos.nombre();
			Fecha fecha = funciones_fecha.PideFechaFabricacion();
			Fecha fUpdate = funciones_fecha.PideFechActualizacion(fecha);
			String marca = funciones_pideDatos.marca(1);
			String color = funciones_pideDatos.color();
			float precio = funciones_pideDatos.precio();
			String DNI = funciones_pideDatos.DNI();
			int pulgadas = funciones_pideDatos.pulgadas();
			int bateria = funciones_pideDatos.duracionBat();
			int pixelCamara = funciones_pideDatos.pixelesCamara();
			String compa�ia = funciones_pideDatos.compa�ia();
			String cpu = funciones_pideDatos.procesador();
			int ram = funciones_pideDatos.ram();
			String sistema = funciones_pideDatos.sistema();
			int capacidad = funciones_pideDatos.capacidad();
			
			 
			 u = new smartphones (cod,nom,fecha,fUpdate,marca,color,DNI,precio,pulgadas, bateria,pixelCamara, compa�ia, cpu, ram,sistema,capacidad);

		}
		if(op == 2){
			
			String cod = Singleton.cod;
			String nom = funciones_pideDatos.nombre();
			Fecha fecha = funciones_fecha.PideFechaFabricacion();
			String marca = funciones_pideDatos.marca(2);
			String color = funciones_pideDatos.color();
			float precio = funciones_pideDatos.precio();
			String DNI = funciones_pideDatos.DNI();
			int pulgadas = funciones_pideDatos.pulgadas();
			String hd = funciones_pideDatos.hd();
			String led = funciones_pideDatos.led();
			String ahorroInteligente = funciones_pideDatos.ahorroInteligente();
			String tipo = funciones_pideDatos.tipoTv();
			int consumo = funciones_pideDatos.consumo();
			String conexion = funciones_pideDatos.conexion();
						 
			 u = new tvs (cod,nom,fecha,marca,color,DNI,precio,pulgadas, hd, led, ahorroInteligente, tipo, consumo, conexion);
			 
			
		}
		 return u;
	}
	
	public static metalWindInstrument pideMetalInstrumentCod () {
		Singleton.cod=funciones_pideDatos.cod();
		
		return new metalWindInstrument (Singleton.cod);
	}
	public static smartphones pideSmartphonesCod () {
		Singleton.cod=funciones_pideDatos.cod();
		
		return new smartphones (Singleton.cod);
	}
	public static tvs pideTvsCod () {
		Singleton.cod=funciones_pideDatos.cod();
		
		return new tvs (Singleton.cod);
	}
	
	public static String ReadProducts (secondHandProducts p){
		
		if(p instanceof metalWindInstrument)
			((metalWindInstrument)p).toString();
		
		if(p instanceof smartphones)
			((smartphones)p).toString();
		
		if(p instanceof tvs)
			((tvs)p).toString();
		
		return p.toString();
	}
	
	public static secondHandProducts UpdateProducts (secondHandProducts p){
		String op = "";
		String[] tipo1 = { "Nombre","Ffabricacion","Marca","Color","Precio","DNI", "Material", "Afinacion", "Boquilla", "Campana","Calibre","Funda","FMantenimiento"};//metalWindInstrument
		String[] tipo2 = { "Nombre","Ffabricacion","Marca","Color","Precio","DNI","Pulgadas","Bateria","Pixeles Camara","Compa�ia","Cpu","RAM","Sistema","Capacidad","FUpdate"};//smartphones
		String[] tipo3 = { "Nombre","Ffabricacion","Marca","Color","Precio","DNI","Pulgadas","HD","LED","Ahorro Inteligente","Tipo TV","Consumo","Conexion"};//tvs
		
		String d;
		int w=0;
		float c = 0.0f;
		Fecha f;
		
		if(p instanceof metalWindInstrument){	
			do{
				op = funciones_menu.combobox("Menu Terciario","Elije que campo quieres cambiar",tipo1);	
				
				if (op.equals(""))
					JOptionPane.showMessageDialog(null,"Volver al menu secundario","Volver",JOptionPane.INFORMATION_MESSAGE);			
				else{
					switch(op){
					
						case "Nombre":
							d = funciones_pideDatos.nombre();
							p.setNombre(d);
							break;
						case "Ffabricacion":
							f = funciones_fecha.PideFechaFabricacion();
							p.setFfabricacion(f);
							break;
						case "Marca":
							d = funciones_pideDatos.marca(0);
							p.setMarca(d);
							break;
						case "Color":
							d = funciones_pideDatos.color();		
							p.setColor(d);
							break;
						case "Precio":
							c = funciones_pideDatos.precio();
							p.setPrecio(c);
							break;
						case "DNI":
							d = funciones_pideDatos.DNI();
							p.setDNI(d);
							break;
						case "Material":
							d = funciones_pideDatos.material();
							((metalWindInstrument) p).setMaterial(d);
							break;
						case "Afinacion": 
							d = funciones_pideDatos.afinacion();
							((metalWindInstrument) p).setAfinacion(d);
							break;
						case "Boquilla":
							d = funciones_pideDatos.boquilla();
							((metalWindInstrument) p).setBoquilla(d);
							break;
						case "Campana":
							c = funciones_pideDatos.campana();
							((metalWindInstrument) p).setCampana(c);
							break;
						case "Calibre":
							d = funciones_pideDatos.calibre();
							((metalWindInstrument) p).setCalibre(d);
							break;
						case "Funda":
							d = funciones_pideDatos.funda();
							((metalWindInstrument) p).setFunda(d);
						break;
						case "FMantenimiento":
							f = funciones_fecha.PideFechMantenimiento(p.getFfabricacion());
							((metalWindInstrument) p).setFMante(f);
						break;
					}
				}
			}while(!op.equals(""));
		}
		
			if(p instanceof smartphones){
				
				do{
					op = funciones_menu.combobox("Menu Terciario","Elije que campo quieres cambiar",tipo2);	
					
					if (op.equals(""))
						JOptionPane.showMessageDialog(null,"Volver al menu secundario","Volver",JOptionPane.INFORMATION_MESSAGE);			
					else{
						switch(op){//Menu3		
							case "Nombre":
								d = funciones_pideDatos.nombre();
								p.setNombre(d);
								break;
							case "Ffabricacion":
								f = funciones_fecha.PideFechaFabricacion();
								p.setFfabricacion(f);
								break;
							case "Marca":
								d = funciones_pideDatos.marca(1);
								p.setMarca(d);
								break;
							case "Color":
								d = funciones_pideDatos.color();		
								p.setColor(d);
								break;
							case "Precio":
								c = funciones_pideDatos.precio();
								p.setPrecio(c);
								break;
							case "DNI":
								d = funciones_pideDatos.DNI();
								p.setDNI(d);
								break;
							case "Pulgadas":
								w = funciones_pideDatos.pulgadas();
								((smartphones) p).setPulgadas(w);
								break;
							case "Bateria": 
								w = funciones_pideDatos.duracionBat();
								((smartphones) p).setDuracionBat(w);
								break;
							case "Pixeles Camara":
								w = funciones_pideDatos.pixelesCamara();
								((smartphones) p).setPixelesCamara(w);
								break;
							case "Compa�ia":
								d = funciones_pideDatos.compa�ia();
								((smartphones) p).setCompa�ia(d);
								break;
							case "Cpu":
								d = funciones_pideDatos.procesador();
								((smartphones) p).setCpu(d);
								break;
							case "RAM":
								w = funciones_pideDatos.ram();
								((smartphones) p).setRam(w);
								break;
							case "Sistema":
								d = funciones_pideDatos.sistema();
								((smartphones) p).setSistema(d);
								break;
							case "Capacidad":
								w = funciones_pideDatos.capacidad();
								((smartphones) p).setCapacidad(w);
								break;
							case "FUpdate":
								f=funciones_fecha.PideFechActualizacion(p.getFfabricacion());
								((smartphones) p).setfUpdate(f);
								break;
						}
					}
				}while(!op.equals(""));
			}
			
				if(p instanceof tvs){
					do{
						op = funciones_menu.combobox("Menu Terciario","Elije que campo quieres cambiar",tipo3);	
						
						if (op.equals(""))
							JOptionPane.showMessageDialog(null,"Volver al menu secundario","Volver",JOptionPane.INFORMATION_MESSAGE);			
						else{
							switch(op){//Menu3
								case "Nombre":
									d = funciones_pideDatos.nombre();
									p.setNombre(d);
									break;
								case "Ffabricacion":
									f = funciones_fecha.PideFechaFabricacion();
									p.setFfabricacion(f);
									break;
								case "Marca":
									d = funciones_pideDatos.marca(2);
									p.setMarca(d);
									break;
								case "Color":
									d = funciones_pideDatos.color();		
									p.setColor(d);
									break;
								case "Precio":
									c = funciones_pideDatos.precio();
									p.setPrecio(c);
									break;
								case "DNI":
									d = funciones_pideDatos.DNI();
									p.setDNI(d);
									break;
								case "Pulgadas":
									w = funciones_pideDatos.pulgadas();
									((tvs) p).setPulgadas(w);
									break;
								case "HD": 
									d = funciones_pideDatos.hd();
									((tvs) p).setHd(d);
									break;
								case "LED":
									d = funciones_pideDatos.led();
									((tvs) p).setLed(d);
									break;
								case "Ahorro Inteligente":
									d = funciones_pideDatos.ahorroInteligente();
									((tvs) p).setAhorroInteligente(d);
									break;
								case "Tipo TV":
									d = funciones_pideDatos.tipoTv();
									((tvs) p).setTipo(d);
									break;
								case "Consumo":
									w = funciones_pideDatos.consumo();
									((tvs) p).setConsumo(w);
									break;
								case "Conexion":
									d = funciones_pideDatos.conexion();
									((tvs) p).setConexion(d);
									break;
							}
						}
					}while(!op.equals(""));	
				}
					return p;
	}
	
	public static secondHandProducts DeleteProducts(secondHandProducts p){
		p = null;
		JOptionPane.showMessageDialog(null,"El producto ha sido borrado satisfactoriamente","Borrado",JOptionPane.INFORMATION_MESSAGE);
		return p;
	}
	
	public static metalWindInstrument CaseMetalWindInstrument (){
		int op = 0;
		
		String[] crud = { "Create","Read","Update","Delete","Order","Volver" };
		
		metalWindInstrument w = null;
		
		do{
			op = funciones_menu.menu("Elije opciones","Menu Secundario",crud);

			if (op==5)
				JOptionPane.showMessageDialog(null,"Volver al Menu principal","Volver",JOptionPane.INFORMATION_MESSAGE);
			
			else{
				switch(op){
					case 0 :
						funciones_create.CreateMetalWindInstrument();
					break;
					case 1 :
						String[] tipo1 = { "Todos","Uno","Volver"};
						int op1 = 0;	
						
						do{
							op1 = funciones_menu.menu("Menu secundario", "Que quieres hacer?",tipo1);
								
							if (op1==2)
								JOptionPane.showMessageDialog(null,"Volviendo...", "Volver",JOptionPane.INFORMATION_MESSAGE);
								
							else{
								switch(op1){
									case 0:
										funciones_read.ReadMetalWindInstruments();
									break;
									case 1:
										funciones_read.ReadMetalWindInstrument();
									break;	
											
								}
							}
								
						}while(op1!=2);	
						break;
					case 2:
						funciones_update.UpdateMetalWindInstrument();
					break;
					case 3:
						funciones_delete.DeleteMetalWindInstrument();
					break;
					case 4:
						funciones_order.orderMetalWindInstrument();;
					break;
				}
			}
		}while(op!=5);
		return w;
			
	}
	
	public static smartphones CaseSmartphones (){
		
		int op = 0;
		
		String[] crud = { "Create","Read","Update","Delete","Order","Volver" };
		
		smartphones w = null;
		
		do{
			op = funciones_menu.menu("Elije opciones","Menu Secundario",crud);

			if (op==5)
				JOptionPane.showMessageDialog(null,"Volver al Menu principal","Volver",JOptionPane.INFORMATION_MESSAGE);
			
			else{
				switch(op){
					case 0 :
						funciones_create.CreateSmartphones();
					break;
					case 1 :
						String[] tipo1 = { "Todos","Uno","Volver"};
						int op1 = 0;	
						
						do{
							op1 = funciones_menu.menu("Menu secundario", "Que quieres hacer?",tipo1);
								
							if (op1==2)
								JOptionPane.showMessageDialog(null,"Volviendo...", "Volver",JOptionPane.INFORMATION_MESSAGE);
								
							else{
								switch(op1){
									case 0:
										funciones_read.ReadSmartphones();
									break;
									case 1:
										funciones_read.ReadSmartphone();
									break;	
								}
							}
								
						}while(op1!=2);	
						
					break;
					case 2:
						funciones_update.UpdateSmartphones();
					break;
					case 3:
						funciones_delete.DeleteSmartphones();
					break;
					case 4:
						funciones_order.orderSmartphones();
					break;
				}
			}
		}while(op!=5);
		return w;
			
	}
	
	public static tvs CaseTvs (){	
		int op = 0;
		
		String[] crud = { "Create","Read","Update","Delete","Order","Volver" };
		
		tvs w = null;
		
		do{
			op = funciones_menu.menu("Elije opciones","Menu Secundario",crud);

			if (op==5)
				JOptionPane.showMessageDialog(null,"Volver al Menu principal","Volver",JOptionPane.INFORMATION_MESSAGE);
			
			else{	
				switch(op){
					case 0 :
						funciones_create.CreateTvs();
					break;
					case 1 :
						String[] tipo1 = { "Todos","Uno","Volver"};
						int op1 = 0;	
						
						do{
							op1 = funciones_menu.menu("Menu secundario", "Que quieres hacer?",tipo1);
								
							if (op1==2)
								JOptionPane.showMessageDialog(null,"Volviendo...", "Volver",JOptionPane.INFORMATION_MESSAGE);
								
							else{
								switch(op1){
									case 0:
										funciones_read.ReadTvs();
									break;
									case 1:
										funciones_read.ReadTv();
									break;	
								}
							}
								
						}while(op1!=2);	
					break;
					case 2:
						funciones_update.UpdateTvs();
					break;
					case 3:
						funciones_delete.DeleteTvs();
					break;
					case 4:
						funciones_order.orderTvs();
					break;
				}
			}
		}while(op!=5);
		return w;
			
	}
}
