import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import Utils.funciones;
import Modules.SecondHandProducts.Utils.funciones_pideDatos;

public class menuFrames {

	public static void login() {
		//falta tindre les finestres estatiques
		
		ArrayList<String> nom = new ArrayList<String>(5);
		ArrayList<String> pass = new ArrayList<String>(5);
		
		JFrame ventana = new JFrame("www.ProductosSegundaMano.es LOGIN BETA 1.0" );
	    ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//para terminar la app
	    ventana.setLocation(450, 200);
		FlowLayout layout = new FlowLayout(FlowLayout.CENTER, 15, 15);//coloca en fila
	    ventana.setLayout(layout);
	 
	     //Creamos el listener para los eventos de cada boton
	     ActionListener escuchador = new ActionListener() {
	        @SuppressWarnings("static-access")
			public void actionPerformed(ActionEvent ae) {
	             JButton boton = (JButton) ae.getSource();
	             String nombreBoton = boton.getText();
	 
	             switch (nombreBoton) {
	                 case "Registros":
	                	if(!nom.isEmpty()){
	                		JOptionPane.showMessageDialog(null, "No puedes registrarte dos veces");
	                	}else{	                
		                    JFrame vtnaDatosRegistro  = new JFrame("Registrar");
		                    vtnaDatosRegistro.setLocation(450, 300);
		                    vtnaDatosRegistro.setLayout(new FlowLayout());
	
		                    //Creamos los componentes
		                    JButton botonEnviar = new JButton("Registrar");
		                    JTextField txtNom  = new JTextField(14);
		                    JTextField txtPasswd  = new JTextField(14);
		                    JLabel etiNom= new JLabel("Nombre: ");
		                    JLabel etiPasswd= new JLabel("Password: ");
		                    
		                    //A�adimos los componentes a la ventana
		                    vtnaDatosRegistro.add(etiNom);
		                    vtnaDatosRegistro.add(txtNom);
		                    vtnaDatosRegistro.add(etiPasswd);
		                    vtnaDatosRegistro.add(txtPasswd);
		                    vtnaDatosRegistro.add(botonEnviar);
		                      	                    
		                    //Configuraciones de la ventana
		                    vtnaDatosRegistro.setSize(500, 95);
		                    vtnaDatosRegistro.setVisible(true);
		                    vtnaDatosRegistro.setResizable(false);
		                    
		                    botonEnviar.addActionListener(new ActionListener(){//posar cada txt en el seu valida string
		                    	public void actionPerformed(ActionEvent e){
		                    	String nombre = txtNom.getText();
		                    	boolean rdo1 = funciones.ValidaStringFrameNom(nombre);//posar un patro de un tope de caracters    
			                    String password = txtPasswd.getText();
			                    boolean rdo2=funciones.ValidaStringFramePass(password);
			                   		                    
			                    if((rdo1)&&(rdo2)){	                    	
			                    	nom.add(nombre);	                    	
			                    	pass.add(password);
			                    	System.out.println("nom: "+nom.toString());
			                    	System.out.println("pass: "+pass.toString());
				                    JOptionPane.showMessageDialog(null, "Muy bien, ahora ingresa para entrar al programa","CORRECTO",JOptionPane.INFORMATION_MESSAGE);
				                    vtnaDatosRegistro.dispose();
			                    }		                    
	
		                    }});
		                    
		                    if(vtnaDatosRegistro.EXIT_ON_CLOSE==-1)
		                    	vtnaDatosRegistro.dispose();
	                    
	                	}	            
	                    break;
	                 case "Ingresar":
	                	 if(nom.isEmpty()){
	                		 JOptionPane.showMessageDialog(null, "Tienes que registrarte primero");
	                	 }else{
		                    JFrame vtnaDatosIngresar  = new JFrame("Ingresar");
		                    vtnaDatosIngresar.setLocation(450, 300);
		                    vtnaDatosIngresar.setLayout(new FlowLayout());

		                    //Creamos los componentes
		                    JButton botEnv = new JButton("Ingresar");
		                    JTextField txtNombre  = new JTextField(14);
		                    JTextField txtPassword  = new JTextField(14);
		                    JLabel etiNombre= new JLabel("Nombre: ");
		                    JLabel etiPassword= new JLabel("Password: ");
		                    
		                    //A�adimos los componentes a la ventana
		                    vtnaDatosIngresar.add(etiNombre);
		                    vtnaDatosIngresar.add(txtNombre);
		                    vtnaDatosIngresar.add(etiPassword);
		                    vtnaDatosIngresar.add(txtPassword);
		                    vtnaDatosIngresar.add(botEnv);
		                      	                    
		                    //Configuraciones de la ventana
		                    vtnaDatosIngresar.setSize(500, 95);
		                    vtnaDatosIngresar.setVisible(true);
		                    vtnaDatosIngresar.setResizable(false);
		                    
		                    botEnv.addActionListener(new ActionListener(){//posar cada txt en el seu valida string
		                    	public void actionPerformed(ActionEvent e){
		                    	String nombre = txtNombre.getText();
			                    String password = txtPassword.getText();
			                   	System.out.println("nom1.1: "+nombre);
			                   	System.out.println("pass1.1: "+password);
			                   	
			                    if((nombre.equals("admin"))&&(password.equals(pass.get(0)))){
			                    	JOptionPane.showMessageDialog(null, "Bienvenido Admin, ahora puedes acceder","CORRECTO",JOptionPane.INFORMATION_MESSAGE);
			                    	vtnaDatosIngresar.dispose();
			                    	ventana.dispose();
			                    	MenuProducts.cargarCrud();		                    	
			                    }else if((nombre.equals("client"))&&(password.equals(pass.get(0)))){
			                    	JOptionPane.showMessageDialog(null, "Bienvenido Cliente, ahora puedes acceder","CORRECTO",JOptionPane.INFORMATION_MESSAGE);
			                    	vtnaDatosIngresar.dispose();
			                    	ventana.dispose();
			                    	menuClient.cargarClient();
			                    	
			                    }else if((nombre.equals("user"))&&(password.equals(pass.get(0)))){
			                    	JOptionPane.showMessageDialog(null, "Bienvenido User, ahora puedes acceder","CORRECTO",JOptionPane.INFORMATION_MESSAGE);
			                    	vtnaDatosIngresar.dispose();
			                    	ventana.dispose();
			                    	menuUser.cargarUser();
			                    	
			                    }
			                    else{
			                    	JOptionPane.showMessageDialog(null, "Nombre o contrase�a invalidas","INCORRECTO",JOptionPane.ERROR_MESSAGE);
			                    	
			                    }
		                    }});
		                    
		                    if(vtnaDatosIngresar.EXIT_ON_CLOSE==-1)
		                    	vtnaDatosIngresar.dispose();
		                    
	                	 }
	                    break;
	             }
	         }
	     };
	 
	     // BOTONES
	     String botones[] = {"Registros", "Ingresar"};
	     for (String titlebutton : botones){
	         JButton boton = new JButton(titlebutton);
	         boton.setSize(new Dimension(200, 150));
	         ventana.add(boton);
	         boton.addActionListener(escuchador);
	     }

	     // END
	     ventana.setSize(500, 100);
	     ventana.setVisible(true);
	     ventana.setResizable(false);
	     
	     
	}

}
