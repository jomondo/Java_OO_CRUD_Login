import javax.swing.JOptionPane;

import Modules.SecondHandProducts.Model.CRUD.funciones_order;
import Modules.SecondHandProducts.Model.CRUD.funciones_read;
import Modules.SecondHandProducts.Model.Dummies.Utils.funciones_dummies;
import Utils.funciones_menu;

public class menuUser {

	public static void cargarUser() {
		JOptionPane.showMessageDialog(null,"Bienvenido a productos de segunda mano","Version: 2.2 by Joan Montes Doria", JOptionPane.INFORMATION_MESSAGE);
		String[] tipo = { "Read Metal Wind Instrument","Read Smartphones","Read TV","Exit"};
		int op = 0;	
		
		funciones_dummies.cargarDummiesUsers();
		
			do{
				op = funciones_menu.menu("Elije opcion","Menu Principal",tipo);	
				if (op==3)
					JOptionPane.showMessageDialog(null,"Bye!!!, deseamos que le haya gustado. Vuelva pronto", "EXIT",JOptionPane.INFORMATION_MESSAGE);
				
				else if (op==-1)
					JOptionPane.showMessageDialog(null,"Tiene que elejir una opcion", "Upss!",JOptionPane.ERROR_MESSAGE);
				
				else{					
					switch(op){
						case 0:
							String[] tipo1 = { "Todos","Uno","Order","Volver"};
							int op1 = 0;	
							
							do{
								op1 = funciones_menu.menu("Menu secundario", "Que quieres hacer?",tipo1);
									
								if (op1==3)
									JOptionPane.showMessageDialog(null,"Volviendo...", "Volver",JOptionPane.INFORMATION_MESSAGE);
									
								else{
									switch(op1){
										case 0:
											funciones_read.ReadMetalWindInstruments();
										break;
										case 1:
											funciones_read.ReadMetalWindInstrument();
										break;
										case 2:
											funciones_order.orderMetalWindInstrument();;
										break;
												
									}
								}
									
							}while(op1!=3);
						break;
							
						case 1:
							String[] tipo2 = { "Todos","Uno","Order","Volver"};
							int op2 = 0;	
							
							do{
								op2 = funciones_menu.menu("Menu secundario", "Que quieres hacer?",tipo2);
									
								if (op2==3)
									JOptionPane.showMessageDialog(null,"Volviendo...", "Volver",JOptionPane.INFORMATION_MESSAGE);
									
								else{
									switch(op2){
										case 0:
											funciones_read.ReadSmartphones();
										break;
										case 1:
											funciones_read.ReadSmartphone();
										break;
										case 2:
											funciones_order.orderSmartphones();
										break;
									}
								}
									
							}while(op2!=3);	
						break;
							
						case 2:
							String[] tipo3 = { "Todos","Uno","Order","Volver"};
							int op3 = 0;	
							
							do{
								op3 = funciones_menu.menu("Menu secundario", "Que quieres hacer?",tipo3);
									
								if (op3==3)
									JOptionPane.showMessageDialog(null,"Volviendo...", "Volver",JOptionPane.INFORMATION_MESSAGE);
									
								else{
									switch(op3){
										case 0:
											funciones_read.ReadTvs();
										break;
										case 1:
											funciones_read.ReadTv();
										break;
										case 2:
											funciones_order.orderTvs();
										break;
									}
								}
									
							}while(op3!=3);
						break;
					}
				}
				
			}while(op!=3);	
	}
}
